<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    public function gudang(){
    	return $this->belongsTo('App\Gudang', 'gudang_id');
    }
    public function satuans(){
    	return $this->belongsTo('App\Satuan', 'id');
    }
}
