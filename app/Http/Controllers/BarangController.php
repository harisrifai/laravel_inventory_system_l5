<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gudang;
use App\Barang;
use App\Satuan;
use Session;

class BarangController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    
    public function index() {
    	$barangs = Barang::all();
    	$gudangs = Gudang::all();
        $satuans = Satuan::all();
    	return view('Barang.index',compact('barangs','gudangs','satuans'));
    }

    public function add(Request $r) {
    	$gudang_id = $r->gudang_id;
        $product_number= $r ->product_number;
    	$nama = $r->nama;
    	$berat = $r->berat;
        $qty = $r->qty;
        $opname = $r->opname;
        $tot_qty = $r->tot_qty;
        $satuan = $r->satuan;
        
        $date_buy = $r->date_buy;
    	$barang = new Barang;
    	$barang->gudang_id = $gudang_id;
        $barang->product_number = $product_number;
    	$barang->nama = $nama;
        $barang->qty = $qty;
        $barang->opname = 0;
    	$barang->berat = $berat;        
        $barang->tot_qty =0;
        $barang->satuan =$satuan;
        $barang->date_buy = $date_buy;
    	$barang->save();

        Session::flash('success_add','Sukses tambah barang!');
    	return redirect()->back();
    }

    public function delete($id) {
    	$barang = Barang::find($id);

    	$barang->delete();
        Session::flash('success_add',"Sukses hapus barang!");
        return redirect()->back();
    }

    public function edit($id) {
        $barangs = Barang::all()->where('id',$id);
        return view('Barang.edit',compact('barangs'));
    }

    public function update(Request $r) {
        $barang = Barang::find($r->id_barang);
        $barang->opname = $r->opname;
        $barang->tot_qty = $r->qty+$r->opname;
        $barang->save();

        Session::flash('success_add',"Sukses update barang!");
        return redirect('barang');
    }
}
