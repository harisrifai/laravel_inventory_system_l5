<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Komputer;
use App\Company;
use App\Dept;
use App\Merek;
use App\Karyawan;
use Carbon\Carbon;
use Session;

class KomputerController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    	//$komputers = Komputer::orderBy('os_build', 'DESC')->get();
        $komputers = Komputer::where('active', 1)->get();
    	//$companys = Company::all();where('name',1);
        $depts = Dept::all();
        $mereks = Merek::all();
        $karyawans = Karyawan::all();
        //$users = Auth\User::all();
    	return view('Komputer.index',compact('komputers','depts','mereks','karyawans'));
    }
    public function add(Request $request) {
    	      
        $this->validate($request, [
            'title' => 'required',
            'ip_pc' => 'required',
            'comp' => 'required',
            'user_id' => 'required',
            'date_buy' => 'required',
            'date_setup' => 'required',
            'model' => 'required',
            'merek_id' => 'required',
            'dept_id' => 'required',
            'os_build' => 'required',
            'remote' => 'required',
            'macaddress' => 'required',
            'email' => 'required',
            'note' => 'required',
            'date_buy' => 'required',
        ]);

        $ip_pc= $request->ip_pc;
        $comp= $request->comp;
        $user_id= $request->user_id;
        $date_buy= $request->date_buy;
        $date_setup= $request->date_setup;
        $model= $request->model;
        $merek_id= $request->merek_id;
        $dept_id= $request->dept_id;
        $os_build= $request->os_build;
        $remote= $request->remote;
        $macaddress= $request->macaddress;
        $email= $request->email;
        $note= $request->note;
        $date_buy = $request->date_buy;


    	$komputer = new Komputer;       
        $komputer->ip_pc=$ip_pc;
        $komputer->comp=$comp;
        $komputer->user_id=$user_id;
        $komputer->date_buy=$date_buy;
        $komputer->date_setup=$date_setup;
        $komputer->model=$model;
        $komputer->merek_id=$merek_id;
        $komputer->dept_id=$dept_id;
        $komputer->os_build=$os_build;
        $komputer->remote=$remote;
        $komputer->macaddress=$macaddress;
        $komputer->email=$email;
        $komputer->note=$note;   
        $komputer->delete_at= Carbon::tomorrow(); 
        $komputer->company_id="AKG"; 
    	$komputer->save();

        Session::flash('success_add','Sukses tambah Komputer!');
    	return redirect()->back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Komputer  $komputer
     * @return \Illuminate\Http\Response
     */
    public function show(Komputer $komputer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Komputer  $komputer
     * @return \Illuminate\Http\Response
     */
    public function edit(Komputer $komputer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Komputer  $komputer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Komputer $komputer)
    {
        $komputer = Barang::find($r->id_barang);
        $komputer->opname = $r->opname;
        $komputer->tot_qty = $r->qty+$r->opname;
        $komputer->save();

        Session::flash('success_add',"Sukses update barang!");
        return redirect('barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Komputer  $komputer
     * @return \Illuminate\Http\Response
     */
    public function delete(Komputer $komputer)
    {
        $komputer = Barang::find($id);
    	$komputer->delete_at = $r->delete_at;
        $komputer->save();
        Session::flash('success_add',"Sukses hapus komputer!");
        return redirect()->back();
    }
}
