<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Notifiable;
class Karyawan extends Model
{
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kar_nik', 'kar_name', 'kar_email'
    ];
}
