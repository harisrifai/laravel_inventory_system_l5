<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komputer extends Model
{
    public function dept(){
    	return $this->belongsToMany('App\Dept', 'id');
    }
    public function merek(){
    	return $this->belongsToMany('App\Merek', 'id');
    }
    public function company(){
    	return $this->belongsToMany('App\Company', 'id');
    }
    public function karyawan(){
    	return $this->belongsToMany('App\Karyawan', 'id');
    }
}