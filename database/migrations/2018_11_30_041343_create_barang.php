<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('gudang_id');
            $table->string('nama');
            $table->integer('qty');
            $table->integer('opname');
            $table->integer('tot_qty');
            $table->string('satuan');
            $table->integer('berat');
            $table->date('date_buy');            
            $table->foreign('gudang_id')->references('id')->on('gudangs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangs');
    }
}
