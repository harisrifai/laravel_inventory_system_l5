<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomputersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komputers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip_pc');	
            $table->string('comp');	
            $table->string('user_id');
            $table->date('date_buy');	
            $table->date('date_setup');	
            $table->string('model');	
            $table->integer('merek_id')->references('id')->on('mereks')->onDelete('cascade');	
            $table->integer('dept_id');	
            $table->string('os_build');	
            $table->string('remote');	
            $table->string('macaddress')->default(0);
            $table->string('email');	
            $table->string('note');
            $table->string('active')->default(0);
            $table->integer('company_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komputers');
    }
}
