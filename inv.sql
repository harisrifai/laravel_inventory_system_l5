/*
 Navicat Premium Data Transfer

 Source Server         : app_db_POS
 Source Server Type    : MariaDB
 Source Server Version : 100407
 Source Host           : localhost:3306
 Source Schema         : inv

 Target Server Type    : MariaDB
 Target Server Version : 100407
 File Encoding         : 65001

 Date: 08/09/2021 22:50:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for barangs
-- ----------------------------
DROP TABLE IF EXISTS `barangs`;
CREATE TABLE `barangs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gudang_id` int(10) UNSIGNED NOT NULL,
  `product_number` int(20) NULL DEFAULT NULL,
  `nama` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `opname` int(11) NULL DEFAULT NULL,
  `tot_qty` int(11) NULL DEFAULT NULL,
  `satuan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `berat` int(11) NULL DEFAULT NULL,
  `date_buy` date NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `barangs_gudang_id_foreign`(`gudang_id`) USING BTREE,
  CONSTRAINT `barangs_gudang_id_foreign_a` FOREIGN KEY (`gudang_id`) REFERENCES `gudangs` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of barangs
-- ----------------------------
INSERT INTO `barangs` VALUES (1, 3, 234, 'AIO', 90, -6, -6, '5', 1, '2010-06-07', '2021-09-07 14:48:23', '2021-09-07 15:23:12');
INSERT INTO `barangs` VALUES (2, 2, 456, 'CPU RAKITAN', 200, 0, 0, '5', 2, '2002-09-08', '2021-09-07 14:50:20', '2021-09-07 14:50:20');
INSERT INTO `barangs` VALUES (4, 1, 222, 'AIO DELL 5400', 200, 0, 0, '5', 1, '2021-06-03', '2021-09-07 14:59:59', '2021-09-07 14:59:59');

-- ----------------------------
-- Table structure for companys
-- ----------------------------
DROP TABLE IF EXISTS `companys`;
CREATE TABLE `companys`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_simbol` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_ceo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_owner` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for depts
-- ----------------------------
DROP TABLE IF EXISTS `depts`;
CREATE TABLE `depts`  (
  `dept_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dept_simbol` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `company_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of depts
-- ----------------------------
INSERT INTO `depts` VALUES (1, 'IT', 'IT', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (2, 'HRD GA', 'HRD', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (5, 'MEKANIK', 'MEKANIK', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (6, 'Opration', 'OPR', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (9, 'ACCOUNTING', 'ACCOUNTING', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (10, 'EXIM', 'EXIM', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (11, 'MD', 'MD', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (12, 'MARKETING', 'MARKETING', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (13, 'DESAIN', 'DESAIN', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (15, 'PURCHASE', 'PURCHASE', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (17, 'SAMPLE', 'SAMPLE', 'akg', NULL, NULL);
INSERT INTO `depts` VALUES (23, 'BEACUKAI', 'BEACUKAI', 'akg', NULL, NULL);

-- ----------------------------
-- Table structure for gudangs
-- ----------------------------
DROP TABLE IF EXISTS `gudangs`;
CREATE TABLE `gudangs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alamat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas` int(10) UNSIGNED NOT NULL,
  `pict` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gudangs
-- ----------------------------
INSERT INTO `gudangs` VALUES (1, 'ANGGUN KREASI', 'GOOD STOCK', 20000, NULL, '2021-09-07 21:37:09', NULL);
INSERT INTO `gudangs` VALUES (2, 'ANGGUN KREASI', 'BAD STOCK', 20000, NULL, '2021-09-07 21:37:09', NULL);
INSERT INTO `gudangs` VALUES (3, 'ANGGUN KREASI', 'DISPOSAL', 20000, NULL, '2021-09-07 21:37:09', NULL);
INSERT INTO `gudangs` VALUES (4, 'ANGGUN KREASI', 'ON USE', 500, NULL, '2021-09-08 03:17:53', '2021-09-08 03:17:53');
INSERT INTO `gudangs` VALUES (5, 'anggun 2', 'Gundang anggun', 20000, NULL, '2021-09-08 05:59:38', '2021-09-08 06:00:00');

-- ----------------------------
-- Table structure for ip_user
-- ----------------------------
DROP TABLE IF EXISTS `ip_user`;
CREATE TABLE `ip_user`  (
  `no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ip_pc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `comp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `date_buy` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `date_setup` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `merek` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `dept_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `office` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cloud` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `local` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `apps` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f16` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f17` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f18` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f19` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f20` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f21` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f22` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f23` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `f24` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ip_user
-- ----------------------------
INSERT INTO `ip_user` VALUES ('1', '192.168.40.2', 'RDP', 'IT', '2016-01-15', '2016', 'PC', 'Rakitan', 'BBM/160013 - 0021/A/V/2016', 'IT', 'Windows 7', 'Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('2', '192.168.40.1', 'Kerio', NULL, '2017-01-15', '2017', 'PC', 'Rakitan', 'BBM/170090 - 0003/A/I/2017', 'IT', 'Kerio', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('3', '192.168.40.99', 'X-IT_1', 'Arifin', '2019-12-05', '2019', 'PC', 'Dell', 'BBM/191099 - 0168/A/XI/2019', 'IT', 'Windows 10', 'Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('4', '192.168.40.10', 'dbakg', 'Database', '2017-08-21', '2017', 'Server', NULL, 'BBM/170437 - 0050/A/VII/2017', 'IT', 'Windows Server', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('5', '192.168.40.9', 'IT', 'Arifin', '2018-05-03', '2018', 'AIO', NULL, ' - ', 'IT', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('6', '192.168.40.100', 'IT', 'RDP', '2016-12-12', '2016', 'PC', NULL, 'BBM/160492 - 0162/A/XII/2016', 'IT', 'Windows 7', 'Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('7', '192.168.40.83', 'IT', 'IT-Haris', '2020-12-12', '2020', 'AIO', NULL, 'BBM/201047 - 0101/A/XII/2020', 'IT', 'Windows 10', 'Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('8', '192.168.40.50', 'showroom', NULL, '2020-12-21', '2020', 'AIO', 'Dell', 'BBM/201047 - 0101/A/XII/2020', 'IT', 'Windows 10', 'Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('9', '192.168.40.200', 'TPB', 'SVR-TPB', '2016-06-12', '2016', 'PC', 'Rakitan', 'BBM/160052 - 0062/A/VI/2016', 'IT', 'Windows 10', 'Ori', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('10', '192.168.40.27', 'A-HRD_1', 'Era ', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160022 - 0037/A/VI/2016', 'HRD', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('11', '192.168.40.22', 'A-HRD_2-PC', 'Sitta', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160014 - 0022/A/VI/2016', 'HRD', 'Windows 7', 'Not Ori', '1', NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('12', '192.168.40.23', 'A-HRD_3', 'Ajeng', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160021 - 0042/A/VI/2016', 'HRD', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('13', '192.168.40.31', 'A-HRD_4', 'Rani', '2016-01-01', '2016', 'PC', 'Rakitan', ' - ', 'HRD', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('14', '192.168.40.25', 'A-GA_1', 'Liatri', '2016-01-03', '2016', 'PC', 'Rakitan', 'BBM/160037 - 0052/A/VI/2016', 'HRD', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('15', '192.168.40.26', 'A_HRD_5', 'Ewin', '2017-01-02', '2017', 'PC', 'Rakitan', 'BBM/170154 - 0016/A/I/2017', 'HRD', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('16', '192.168.40.30', 'A-SECURITY', 'Teguh', '3/15/2016', '2016', 'PC', 'Rakitan', 'BBM/160021 - 0042/A/VI/2016', 'GA', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('17', '192.168.40.28', 'A-HRD_6', 'Vero', '2017-12-22', '2017', 'AIO', NULL, 'BBM/170712 - 0134/A/X/2017', 'GA', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('18', '192.168.40.29', 'A-HRD_7', 'Putri', '2018-02-16', '2018', 'AIO', NULL, 'BBM/180059 - 0011/A/I/2018', 'HRD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('19', '192.168.40.32', 'A-HRD_8', 'Devi', '2018-05-20', '2018', 'AIO', NULL, 'BBM/180411 - 0109/A/IV/2018', 'HRD', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('20', '192.168.40.34', 'F-KLINIK_1', 'Klinik', '2020-01-31', '2020', 'AIO', 'Dell', 'BBM/200073 - 0006/A/I/2020', 'HRD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('21', '192.168.40.42', 'T-IE_2', 'Evelin', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160145 - 0083/A/VII/2016', 'IE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('22', '192.168.40.43', 'T-IE_3', 'Dian', '2017-12-13', '2017', 'AIO', NULL, 'BBM/170710 - 0133/A/X/2017', 'IE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('23', '192.168.40.44', 'T-IE_1', 'Thomas', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160144 - 0082/A/VII/2016', 'IE', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('24', '192.168.40.45', 'T-IE_4', 'Cahyo', '2018-02-16', '2018', 'AIO', NULL, 'BBM/180143 - 0034/A/II/2018', 'IE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('25', '192.168.40.46', 'T-IE_5', 'dedi', '2018-08-28', '2018', 'AIO', NULL, 'BBM/180778 - 0230/A/VIII/2018', 'IE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('26', '192.168.40.47', 'T-IE_6', 'agus', '2018-09-15', '2018', 'AIO', NULL, 'BBM/180903 - 0247/A/IX/2018', 'IE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('27', '192.168.40.48', 'T-IE_7', 'nurul', '2018-11-27', '2018', 'AIO', NULL, 'BBM/181181 - 0315/A/XI/2018', 'IE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('28', '192.168.40.65', 'O-MEKANIK_1', 'Mekanik', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160113 - 0077/A/VII/2016', 'MEKANIK', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('29', '192.168.40.71', 'M-WH_1', 'Sugeng', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160112 - 0076/A/VII/2016', 'WAREHOUSE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('30', '192.168.40.72', 'M-WH_2', 'Anah', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160287 - 0124/A/IX/2016', 'WAREHOUSE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('31', '192.168.40.73', 'M-WH_3', 'Vena', '2017-11-03', '2017', 'AIO', NULL, 'BBM/170711 - 0131/A/X/2017', 'WAREHOUSE', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('32', '192.168.40.74', 'M-WH_4', 'Ika', '2017-12-27', '2017', 'AIO', NULL, 'BBM/170884 - 0189/A/XII/2017', 'WAREHOUSE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('33', '192.168.40.75', 'M-WH_5', 'Alfi', '3/16/2018', '2018', 'AIO', NULL, 'BBM/180245 - 0065/A/III/2018', 'WAREHOUSE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('34', '192.168.40.76', 'M-WH_6', 'Siti', '3/23/2018', '2018', 'AIO', NULL, 'BBM/180251 - 0052/A/II/2018', 'WAREHOUSE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('35', '192.168.40.77', 'M-WH_7', 'Tanto', '2018-10-10', '2018', 'AIO', NULL, 'BBM/180944 - 0278/A/X/2018', 'WAREHOUSE', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('36', '192.168.40.78', 'M-WH_8', 'Dipta', '10/15/2018', '2018', 'AIO', NULL, 'BBM/180943 - 0271/A/IX/2018', 'WAREHOUSE', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('37', '192.168.40.79', 'M-WH_9', 'Dea', '11/19/2018', '2018', 'AIO', NULL, 'BBM/181166 - 0264/A/IX/2018', 'WAREHOUSE', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('38', '192.168.40.80', 'M-WH_10', 'Dyah', '1/29/2019', '2019', 'AIO', 'Lenovo', 'BBM/190061 - 0008/A/I/2019', 'WAREHOUSE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('39', '192.168.40.64', 'M-WH_11', 'Yuli', '1/29/2019', '2019', 'AIO', 'Lenovo', 'BBM/190060 - 0004/A/I/2019', 'WAREHOUSE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('40', '192.168.40.63', 'M-WH_12', 'Ika', '3/27/2019', '2019', 'AIO', 'Lenovo', 'BBM/190287 - 0045/A/III/2019', 'WAREHOUSE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('41', '192.168.40.81', 'Z-COMPONEN_1', 'Vera', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160287 - 0124/A/IX/2016', 'COMPONENT', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('42', '192.168.40.92', 'B-PROD_1', 'Dian', '10/28/2016', '2016', 'PC', 'Rakitan', 'BBM/160287 - 0124/A/IX/2016', 'PRODUKSI', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('43', '192.168.40.93', 'B-PROD_3', 'Karsim', '12/28/2017', '2017', 'AIO', NULL, 'BBM/170886 - 0188/A/XII/2017', 'PRODUKSI', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('44', '192.168.40.54', 'B-PROD_FM', 'Balbir', '2018-02-22', '2018', 'AIO', NULL, 'BBM/180169 - 0030/A/II/2018', 'PRODUKSI', 'Windows 10', 'Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('45', '192.168.40.94', 'B-PROD_4', 'melia', '2019-07-29', '2019', 'AIO', 'Lenovo', 'BBM/190673 - 0111/A/VII/2019', 'PRODUKSI', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('46', '192.168.42.186', 'B-PROD_5', 'adm', '2021-06-21', '2021', 'AIO', 'Dell', 'BBM/210643 - 0052/A/VI/2021', 'PRODUKSI', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('47', '192.168.40.39', 'C-ACC_3', 'Arif', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160287 - 0124/A/IX/2016', 'ACCOUNTING', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('48', '192.168.40.102', 'C-ACC_1', 'Ummi', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160146 - 0084/A/VII/2016', 'ACCOUNTING', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('49', '192.168.40.101', 'C-ACC_2', 'Philea', '2016-12-18', '2016', 'PC', 'Rakitan', 'BBM/160114 - 0074/A/VII/2016', 'ACCOUNTING', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('50', '192.168.40.104', 'C-ACC_4', 'Dika', '2017-12-22', '2017', 'AIO', NULL, 'BBM/170846 - 0136/A/X/2017', 'ACCOUNTING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('51', '192.168.40.105', 'C-ACC_5', 'Andaru', '2018-07-07', '2018', 'AIO', NULL, 'BBM/180649 - 0188/A/VII/2018', 'ACCOUNTING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('52', '192.168.40.37', 'C-ACC_6', 'Titi', '2018-10-26', '2018', 'AIO', NULL, 'BBM/180991 - 0290/A/X/2018', 'ACCOUNTING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('53', '192.168.40.38', 'C-ACC_7', 'Taxs', '2019-04-08', '2019', 'AIO', 'Lenovo', 'BBM/190353 - 0052/A/III/2019', 'ACCOUNTING', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('54', '192.168.40.40', 'C_ACC_8', 'Fauzie', '2019-12-31', '2019', 'AIO', 'Dell', 'BBM/191196 - 0177/A/XII/2019', 'ACCOUNTING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('55', '192.168.40.111', 'E-EXIM_1', 'Heni', '7/21/2017', '2017', 'PC', 'Rakitan', 'BBM/170241 - 0024/A/II/2017', 'EXIM', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('56', '192.168.40.112', 'E-EXIM_2', 'Yeni', '7/21/2017', '2017', 'PC', 'Rakitan', 'BBM/170241 - 0024/A/II/2017', 'EXIM', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('57', '192.168.40.113', 'E-EXIM_3', 'PEB', '2017-03-11', '2017', 'AIO', NULL, 'BBM/170848 - 0183/A/XI/2017', 'EXIM', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('58', '192.168.40.226', 'E-EXIM_4', 'Tanjung', '2017-04-11', '2017', 'AIO', NULL, 'BBM/170731 - 0140/A/XI/2017', 'EXIM', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('59', '192.168.40.115', 'E-EXIM_5', 'Nanda', '2/25/2018', '2018', 'AIO', NULL, 'BBM/180144 - 0032/A/II/2018', 'EXIM', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('60', '192.168.40.110', 'E-EXIM_6', 'haris', '2018-03-06', '2018', 'AIO', NULL, 'BBM/180271 - 0093/A/III/2018', 'EXIM', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('61', '192.168.40.109', 'E-EXIM_7', 'MD/Milenea', '2018-03-06', '2018', 'AIO', NULL, 'BBM/180215 - 0062/A/II/2018', 'EXIM', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('62', '192.168.40.108', 'E-EXIM_8', 'Kenzo', '2018-07-19', '2018', 'AIO', NULL, 'BBM/180650 - 0191/A/VII/2018', 'EXIM', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('63', '192.168.40.107', ' E-EXIM_9', 'budi', '2019-01-17', '2019', 'AIO', 'Lenovo', 'BBM/190101 - 0019/A/I/2019', 'EXIM', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('64', '192.168.40.82', 'E-EXIM_10', 'yudi', '2019-10-29', '2019', 'AIO', 'Lenovo', 'BBM/190986 - 0161/A/X/2019', 'EXIM', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('65', '192.168.40.62', 'E-EXIM_11', 'vika', '2021-01-14', '2021', 'AIO', 'Dell', 'BBM/210038 - 0111/A/XII/2020', 'EXIM', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('66', '192.168.40.116', 'Totok', 'D-MD_1', '2017-11-09', '2017', 'PC', 'Rakitan', 'From Ameya - From Ameya', 'MD', 'Windows 7', 'Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('67', '192.168.40.117', 'April', 'D-MD_2', '2017-11-09', '2017', 'PC', 'Rakitan', 'From Ameya - From Ameya', 'MD', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('68', '192.168.40.118', 'faulin', 'D-MD_3', '2017-11-09', '2017', 'PC', 'Rakitan', 'From Ameya - From Ameya', 'MD', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('69', '192.168.40.119', 'Kendi', 'D-MD_4', '2017-11-09', '2017', 'PC', NULL, ' - ', 'MD', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('70', '192.168.40.120', 'Geovani', 'D-MD_5', '2017-11-09', '2017', 'PC', 'Rakitan', 'From Ameya - From Ameya', 'MD', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('71', '192.168.40.122', 'Dyaning', 'D-MD_6', '2018-03-01', '2018', 'PC', NULL, ' - ', 'MD', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('72', '192.168.40.123', 'Arya', 'D-MD_7', '2018-03-01', '2018', 'PC', 'Rakitan', 'From Ameya - From Ameya', 'MD', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('73', '192.168.40.124', 'Rinda', 'D-MD_8', '2018-03-01', '2018', 'PC', 'Rakitan', 'From Ameya - From Ameya', 'MD', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('74', '192.168.40.149', 'Amy', 'D-MD_10', '2018-05-03', '2018', 'AIO', NULL, 'BBM/180459 - 0126/A/V/2018', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('75', '192.168.40.150', 'Dicky', 'D-MD_11', '2018-05-07', '2018', 'AIO', NULL, 'BBM/180412 - 0099/A/IV/2018', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('76', '192.168.40.151', 'Oca', 'D-MD_12', '2018-05-15', '2018', 'AIO', NULL, 'BBM/180418 - 0112/A/IV/2018', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('77', '192.168.40.152', 'Atta', 'MD_Atta', '2018-06-04', '2018', 'AIO', NULL, 'BBM/180587 - 0154/A/VI/2018', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('78', '192.168.40.153', 'Agnes', 'D-MD_13', '2018-06-05', '2018', 'AIO', NULL, 'BBM/180459 - 0126/A/V/2018', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('79', '192.168.40.154', 'Yanti', 'D-MD_14', '2018-07-07', '2018', 'AIO', NULL, 'BBM/180614 - 0168/A/VI/2018', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('80', '192.168.40.155', 'Deris', 'D-MD_15', '2018-07-19', '2018', 'AIO', NULL, 'BBM/180615 - 0159/A/VI/2018', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('81', '192.168.40.156', 'ex Darsono', 'D-MD_16', '2018-08-03', '2018', 'AIO', NULL, 'BBM/180699 - 0208/A/VII/2018', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('82', '192.168.40.157', 'Trias', 'D-MD_17', '2019-04-29', '2019', 'AIO', 'Lenovo', 'BBM/190405 - 0065/A/IV/2019', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('83', '192.168.40.158', 'Herna', 'D-MD_18', '2019-07-29', '2019', 'AIO', 'Lenovo', 'BBM/190671 - 0107/A/VII/2019', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('84', '192.168.40.70', 'Gema', 'D-MD_19', '2019-08-08', '2019', 'AIO', 'Lenovo', 'BBM/190693 - 0119/A/VII/2019', 'md', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('85', '192.168.40.247', 'Tri', 'D-MD_20', '2019-08-12', '2019', 'AIO', 'Lenovo', 'BBM/190729 - 0123/A/VIII/2019', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('86', '192.168.40.227', 'Azura', 'D-DMD_21', '2020-01-29', '2020', 'AIO', 'Dell', 'BBM/200076 - 0010/A/I/2020', 'MD', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('87', '192.168.40.122', 'DYANING', 'D-MD_22', '2020-12-18', '2020', 'AIO', 'Dell', 'BBM/201038 - 0100/A/XII/2020', 'md', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('88', '192.168.41.249', 'Ella', 'D-MD_23', '2021-02-15', '2021', 'AIO', NULL, 'From Ameya - From Ameya', 'md', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('89', '192.168.41.71', 'FRANDO', 'D-MD_24', '2021-06-21', '2021', 'AIO', 'Dell', 'BBM/210578 - 0039/A/V/2021', 'MD', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('90', '192.168.41.210', 'Savira', 'D-MD-25', '2021-06-21', '2021', 'AIO', 'Dell', 'BBM/210605 - 0045/A/VI/2021', 'MD', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('91', '192.168.42.0', 'Maike', 'D-MD-26', '2021-08-04', '2021', 'AIO', NULL, ' - ', 'md', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('92', '192.168.42.0', 'Marketing', 'Dita', '2018-06-04', '2018', NULL, NULL, ' - ', 'MARKETING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('93', '192.168.42.0', 'Marketing', 'Septiana', '2018-06-04', '2018', NULL, NULL, ' - ', 'MARKETING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('94', '192.168.40.86', 'H-MARKETING_2', 'Ana', '6/28/2018', '2018', 'AIO', NULL, 'BBM/180503 - 0138/A/V/2018', 'MARKETING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('95', '192.168.40.85', 'H-MARKETING_1', 'Dita', '6/28/2018', '2018', 'AIO', NULL, 'BBM/180587 - 0154/A/VI/2018', 'MARKETING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('96', '192.168.40.87', 'H-MARKETING_3', 'Nana ', '12/14/2018', '2018', 'AIO', NULL, 'BBM/181255 - 0334/A/XII/2018', 'MARKETING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('97', '192.168.40.88', 'H-MARKETING_4', 'Fenty', '1/18/2019', '2019', 'AIO', 'Lenovo', 'BBM/190100 - 0010/A/I/2019', 'MARKETING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('98', '192.168.40.41', 'Abhisek', 'Abhisek', '2018-01-12', '2018', 'AIO', NULL, 'BBM/180773 - 0229/A/VIII/2018', 'MARKETING', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('99', '192.168.41.88', 'H-MARKETING_5', 'Rina', '6/21/2021', '2021', 'AIO', 'Dell', 'BBM/210642 - 0053/A/VI/2021', 'MARKETING', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('100', '192.168.40.125', 'G-DESIGN_1/D-MD_22', 'Nova', '2017-08-15', '2017', 'NUC', NULL, 'BBM/170506 - 0066/A/VIII/2017', 'DESAIN', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('101', '192.168.40.126', 'G-DESAIN_1', 'VERO', '2017-08-19', '2017', 'AIO', NULL, 'BBM/170847 - 0174/A/XI/2017', 'DESAIN', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('102', '192.168.40.127', 'G-DESAIN_2', 'Yunita', '2017-12-22', '2017', 'AIO', NULL, 'BBM/170919 - 0200/A/XII/2017', 'DESAIN', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('103', '192.168.40.128', 'G-DESAIN_5', 'Nugroho', '3/19/2018', '2018', 'PC', 'Rakitan', 'From Ameya - From Ameya', 'DESAIN', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('104', '192.168.40.129', 'G-DESAIN_3', 'Patty', '5/22/2018', '2018', NULL, NULL, ' - ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('105', '192.168.40.129', 'G-DESAIN_3', 'Patty', '9/14/2018', '2018', 'AIO', NULL, 'BBM/180778 - 0230/A/VIII/2018', 'DESAIN', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('106', '192.168.40.229', 'G-DESAIN_4', 'nova', '2020-02-24', '2020', 'AIO', 'Dell', 'BBM/200144 - 0014/A/II/2020', 'DESAIN', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('107', '192.168.40.239', 'G-DESAIN_6', '3D', '5/28/2020', '2020', 'PC', 'Dell', 'BBM/200374 - 0033/A/V/2020', 'DESAIN', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('108', '192.168.42.239', 'G-DESAIN_7', '3D', '5/28/2020', '2020', 'PC', 'Dell', 'BBM/200374 - 0033/A/V/2020', 'DESAIN', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('109', '192.168.40.130', 'P-PURCHASE_1', 'Wiji', '2017-07-12', '2017', 'AIO', NULL, 'BBM/170843 - 0170/A/XI/2017', 'PURCHASE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('110', '192.168.40.137', 'Purchase', 'Much Fajar', '2018-01-03', '2018', 'PC', 'Rakitan', 'From Ameya - From Ameya', 'PURCHASE', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('111', '192.168.40.139', 'P-PURCHASE_2', 'Haryo', '2018-05-17', '2018', 'AIO', NULL, 'BBM/180417 - 0111/A/IV/2018', 'PURCHASE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('112', '192.168.40.140', 'P-PURCHASE_3', 'Prajakti', '2018-09-18', '2018', 'AIO', NULL, 'BBM/180902 - 0261/A/IX/2018', 'PURCHASE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('113', '192.168.40.138', 'P-PURCHASE_4', 'Diksan', '2018-09-18', '2018', 'AIO', NULL, 'BBM/180849 - 0246/A/IX/2018', 'PURCHASE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('114', '192.168.40.136', 'P-PURCHASE_5', 'luluc', '2018-11-19', '2018', 'AIO', NULL, 'BBM/181168 - 0267/A/IX/2018', 'PURCHASE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('115', '192.168.40.49', 'P-PURCHASE_6', 'Indri', '2018-11-19', '2018', 'AIO', NULL, 'BBM/181167 - 0300/A/XI/2018', 'PURCHASE', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('116', '192.168.40.131', 'Y-CUTTING_1', 'Dewi ', '2016-12-16', '2016', 'PC', 'Rakitan', 'BBM/160371 - 0142/A/X/2016', 'CUTTING', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('117', '192.168.40.132', 'Y-CUTTING_2', 'Aji', '2016-12-16', '2016', 'PC', 'Rakitan', 'BBM/160371 - 0142/A/X/2016', 'CUTTING', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('118', '192.168.40.133', 'Y-CUTTING_3', 'Hari ', '2016-12-16', '2016', 'PC', 'Rakitan', 'BBM/160371 - 0142/A/X/2016', 'CUTTING', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('119', '192.168.40.134', 'Y-CUTTING_4', 'Bundle', '2018-05-24', '2018', 'AIO', NULL, 'BBM/180458 - 0125/A/V/2018', 'CUTTING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('120', '192.168.40.135', 'Y-CUTTING_5', 'Dina', '2018-08-03', '2018', 'AIO', NULL, 'BBM/180699 - 0208/A/VII/2018', 'CUTTING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('121', '192.168.42.13', 'LASER', 'Venda', '2020-10-21', '2020', 'PC', 'Rakitan', 'BBM/200800 - 0077/A/X/2020', NULL, 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('122', '192.168.40.141', 'K-QC_1', 'Niken', '2016-12-16', '2016', 'PC', 'Rakitan', 'BBM/160371 - 0142/A/X/2016', 'QC', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('123', '192.168.40.142', 'K-QC_2', 'Temi', '2016-12-16', '2016', 'PC', 'Rakitan', 'BBM/160287 - 0124/A/IX/2016', 'QC', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('124', '192.168.40.143', 'K-QC_3', 'Wanto', '2016-12-16', '2016', 'PC', 'Rakitan', 'BBM/160287 - 0124/A/IX/2016', 'QC', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('125', '192.168.40.144', 'K-QC_4', 'Lilis', '2017-09-08', '2017', 'NUC', NULL, 'BBM/170507 - 0067/A/VIII/2017', 'QC', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('126', '192.168.40.145', 'K-QCBUYER_1', 'Tari', '2017-12-19', '2017', 'AIO', NULL, 'BBM/170918 - 0196/A/XII/2017', 'QC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('127', '192.168.40.148', 'LAB', 'Shena', '2018-03-02', '2018', 'AIO', NULL, 'BBM/180194 - 0045/A/II/2018', 'QC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('128', '192.168.40.147', 'K-QC_5', 'adm', '2018-09-14', '2018', 'AIO', NULL, 'BBM/180846 - 0262/A/IX/2018', 'QC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('129', '192.168.40.96', 'K-QC_6', 'Niken', '2018-10-10', '2018', 'AIO', NULL, 'BBM/180992 - 0287/A/X/2018', 'QC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('130', '192.168.40.98', 'K-QC_8', 'Wiwin', '2018-10-15', '2018', 'AIO', NULL, 'BBM/180993 - 0288/A/X/2018', 'QC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('131', '192.168.40.97', 'K-QC_9', 'Fabric', '2018-11-19', '2018', 'AIO', NULL, 'BBM/181196 - 0325/A/XI/2018', 'QC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('132', '192.168.41.112', 'K-QC_10', 'sot', '2021-01-14', '2021', 'AIO', 'Dell', 'BBM/210037 - 0104/A/XII/2020', 'QC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('133', '192.168.40.243', 'K-IQT_1', 'Sigit', '2021-06-17', '2021', 'AIO', 'Dell', 'BBM/210645 - 0051/A/VI/2021', 'QC', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('134', '192.168.40.90', 'B-PROD_2', 'Jonno', '2016-05-04', '2016', 'PC', 'Rakitan', 'BBM/160190 - 0064/A/VI/2016', 'PRODUKSI', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('135', '192.168.40.162', 'W-PACKING_2', 'Nur', '2016-05-04', '2016', 'PC', 'Rakitan', 'BBM/160144 - 0082/A/VII/2016', 'FINISHING', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('136', '192.168.40.163', 'W-PACKING_3', 'Brina', '2017-12-27', '2017', 'AIO', NULL, 'BBM/170885 - 0192/A/XII/2017', 'FINISHING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('137', '192.168.40.164', 'W-PACKING_4', 'Rista', '2018-03-02', '2018', 'AIO', NULL, 'BBM/180195 - 0046/A/II/2018', 'FINISHING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('138', '192.168.40.160', 'W-PACKING_5', 'Novia', '2018-05-03', '2018', 'AIO', NULL, 'BBM/180416 - 0108/A/IV/2018', 'FINISHING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('139', '192.168.40.159', 'W-PACKING_7', 'wulan', '2019-06-29', '2019', 'AIO', 'Lenovo', 'BBM/190672 - 0098/A/VI/2019', 'FINISHING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('140', '192,168,40,165', 'W-PACKING_8', 'nur', '2019-07-29', '2019', 'AIO', 'Lenovo', 'BBM/190669 - 0100/A/VI/2019', 'FINISHING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('141', '192.168.40.67', 'W-WASHING_1', 'Aan', '2019-07-29', '2019', 'AIO', 'Lenovo', 'BBM/190670 - 0103/A/VII/2019', 'FINISHING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('142', '192.168.40.16', 'W-BARCODE', 'barcode', '2020-10-12', '2020', 'PC', 'Rakitan', 'BBM/200823 - 0081/A/X/2020', 'FINISHING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('143', '192.168.40.190', 'I-SAMPLE_2', 'Rizky', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170726 - 0129/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('144', '192.168.40.188', 'I-POLA_5', 'Marino ', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170481 - 0056/A/VII/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('145', '192.168.40.187', 'I-PATTERN_1', NULL, '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170726 - 0129/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('146', '192.168.40.186', 'ADMSAMPLE', 'Lora', '2017-11-12', '2017', 'AIO', NULL, 'BBM/170844 - 0172/A/XI/2017', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('147', '192.168.40.184', 'I-MARKER_1', 'Tugini', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170726 - 0129/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('148', '192.168.40.182', 'I-CAD_2', 'Tri', '12/16/2016', '2016', 'PC', 'Rakitan', 'BBM/160493 - 0162/A/XII/2016', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('149', '192.168.40.181', 'I-CAD_1', 'Restu', '12/16/2016', '2016', 'PC', 'Rakitan', 'BBM/160542 - 0166/A/XII/2016', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('150', '192.168.40.180', 'I-ADMPATTERN', 'Erna', '2017-05-06', '2017', 'PC', 'Rakitan', 'BBM/170446 - 0054/A/VII/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, ' ', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('151', '192.168.40.189', 'I-POLA_4', 'Windy', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170726 - 0129/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('152', '192.168.40.177', 'Marker', 'Yuli', '12/21/2017', '2017', 'PC', 'Rakitan', 'BBM/170725 - 0128/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('153', '192.168.40.176', 'I-CAD_3', 'Singgih', '2017-10-11', '2017', 'PC', 'Rakitan', 'BBM/170726 - 0129/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('154', '192.168.40.175', 'I-SAMPLE_4', 'Budi', '2017-10-11', '2017', 'PC', 'Rakitan', 'BBM/170726 - 0129/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('155', '192.168.40.174', 'I-SAMPLE_3', 'Indar', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170725 - 0128/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('156', '192.168.40.173', 'I-POLA_3', 'Bayu', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170725 - 0128/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('157', '192.168.40.172', 'I-POLA_2', 'Heri', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170725 - 0128/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('158', '192.168.40.171', 'I-POLA_1', 'Irwan', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170724 - 0127/A/X/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('159', '192.168.40.170', 'I-Marker_2', 'nurw', '2017-03-11', '2017', 'PC', 'Rakitan', 'BBM/170802 - 0156/A/XI/2017', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('160', '192.168.40.169', 'I-SAMPLE_1', 'Dendi', '2018-02-22', '2018', 'PC', 'Rakitan', 'BBM/180129 - 0021/A/I/2018', 'SAMPLE', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('161', '192.168.40.168', 'I-SAMPLE_15', 'Dini', '2017-03-11', '2017', 'AIO', NULL, 'BBM/170917 - 0195/A/XII/2017', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('162', '192.168.40.206', 'I-ADMSAMPLE_2', 'Riska', '2018-03-13', '2018', 'AIO', NULL, 'BBM/180215 - 0062/A/II/2018', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('163', '192.168.40.146', 'K-QA_1', 'Imam', '2018-03-11', '2018', 'AIO', NULL, 'BBM/180226 - 0066/A/III/2018', 'QA Sample', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('164', '192.168.40.167', 'I-SAMPLE_5', 'Fitri', '2018-06-04', '2018', 'PC', 'Rakitan', 'BBM/180508 - 0141/A/V/2018', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('165', '192.168.40.166', 'I-SAMPLE_6', 'Udin', '2018-06-04', '2018', 'PC', 'Rakitan', 'BBM/180508 - 0141/A/V/2018', 'SAMPLE', 'Windows 7', 'Not Ori', NULL, '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('166', '192.168.40.201', 'I-SAMPLE_7', 'arfi', '2018-08-03', '2018', 'AIO', NULL, 'BBM/180700 - 0205/A/VII/2018', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('167', '192.168.40.202', 'I-SAMPLE_8', 'Nurul', '2018-08-03', '2018', 'AIO', NULL, 'BBM/180701 - 0195/A/VII/2018', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('168', '192.168.40.203', 'I-SAMPLE_9', 'heru', '2018-08-07', '2018', 'PC', 'Rakitan', 'BBM/180717 - 0209/A/VIII/2018', 'SAMPLE', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('169', '192.168.40.204', 'I-SAMPLE_10', 'Rita', '2018-08-07', '2018', 'PC', 'Rakitan', 'BBM/180717 - 0209/A/VIII/2018', 'SAMPLE', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('170', '192.168.40.205', 'I-SAMPLE_11', 'Alfi', '2018-09-26', '2018', 'AIO', NULL, 'BBM/180894 - 0272/A/IX/2018', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('171', '192.168.40.208', 'I-SAMPLE_12', 'Puput', '2018-10-03', '2018', 'PC', 'Rakitan', 'BBM/180984 - 0265/A/IX/2018', 'SAMPLE', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('172', '192.168.40.207', 'I-SAMPLE_13', 'cutting', '2018-11-19', '2018', 'AIO', NULL, 'BBM/181167 - 0300/A/XI/2018', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('173', '192.168.40.209', 'I-SAMPLE_14', 'satrio', '2019-06-10', '2019', 'PC', 'Rakitan', 'BBM/190504 - 0073/A/V/2019', 'SAMPLE', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('174', '192.168.40.178', 'I-SAMPLE_15', 'Elis', '2019-06-10', '2019', 'PC', 'Rakitan', 'BBM/190504 - 0073/A/V/2019', 'SAMPLE', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('175', '192.168.40.', 'I-SAMPLE_16', 'Alfa', '2019-08-21', '2019', 'AIO', 'Lenovo', 'BBM/190766 - 0129/A/VIII/2019', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('176', '192.168.40.235', 'I-SAMPLE_17', 'tessa', '2019-10-01', '2019', 'AIO', 'Lenovo', 'BBM/190895 - 0131/A/VIII/2019', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('177', '192.168.40.223', 'I-SAMPLE_18', 'P putut', '2020-01-31', '2020', 'AIO', NULL, 'BBM/200077 - 0009/A/I/2020', 'SAMPLE', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('178', '192.168.40.', 'I-SAMPLE_19', 'Retno', '2020-01-31', '2020', 'PC', 'Rakitan', 'BBM/200087 - 0008/A/I/2020', 'SAMPLE', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('179', '192.168.40.231', 'I-SAMPLE_20', 'Nur', '2021-06-30', '2021', 'PC', 'Dell', 'BBM/210694 - 0060/A/VI/2021', 'SAMPLE', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('180', '192.168.40.191', 'S-PPIC_1', 'Yani', '12/16/2016', '2016', 'PC', 'Rakitan', ' - ', 'PPIC', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('181', '192.168.40.192', 'S-PPIC_2/', 'Ratna ', '12/16/2016', '2016', NULL, NULL, ' - ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('182', '192.168.40.225', 'S-PPIC_3/K-QC_10', 'siwi', '2018-03-11', '2018', 'AIO', NULL, ' - ', 'PPIC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('183', '192.168.40.194', 'S-PPIC_4', 'Nurul', '11/20/2017', '2017', 'AIO', NULL, 'BBM/170740 - 0141/A/XI/2017', 'PPIC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('184', '192.168.40.195', 'S-PPIC_5/QC', 'Isti', '12/13/2017', '2017', 'AIO', NULL, 'BBM/170845 - 0135/A/X/2017', 'PPIC', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('185', '192.168.40.161', 'W-PACKING_1', 'Ardi ', '12/16/2016', '2016', 'PC', 'Rakitan', ' - ', 'FINISHING', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, ' ', NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('186', '192.168.40.196', 'S-PPIC_6', 'Rina', '2018-06-05', '2018', 'AIO', NULL, 'BBM/180504 - 0142/A/V/2018', 'PPIC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('187', '192.168.40.197 ', 'S-PPIC_7', 'Hepi', '2018-10-22', '2018', 'AIO', NULL, 'BBM/181030 - 0297/A/X/2018', 'PPIC', 'Windows 10', 'Not Ori', '1', NULL, NULL, NULL, NULL, ' ', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('188', '192.168.40.198', 'S-PPIC_8', 'Mifta', '2018-10-22', '2018', 'AIO', NULL, 'BBM/181016 - 0287/A/X/2018', 'PPIC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('189', '192.168.40.199', 'S-PPIC_9', 'Arvi', '2018-10-22', '2018', 'AIO', NULL, 'BBM/181016 - 0287/A/X/2018', 'PPIC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('190', '192.168.40.66', 'S-PPIC_10', 'Iin', '2018-12-16', '2018', 'AIO', NULL, 'BBM/181256 - 0332/A/XII/2018', 'PPIC', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('191', '192.168.40.192', 'N-PLANNING_1', 'Ratna ', '2019-10-01', '2019', 'PC', 'Rakitan', 'BBM/190896 - 0147/A/IX/2019', 'PLANNING', 'Windows 10', 'Not Ori', NULL, '1', ' ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('192', '192.168.40.198', 'N-PLANNING_2', 'Wury', '2019-10-01', '2019', 'PC', 'Rakitan', 'BBM/190896 - 0147/A/IX/2019', 'PLANNING', 'Windows 10', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('193', '192.168.40.61', 'O-MTN_1', 'Adm. Mtn', '2016-12-16', '2016', 'PC', 'Rakitan', 'BBM/160407 - 0146/A/XI/2016', 'UTILITY', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('194', '192.168.40.251', 'BEA_CUKAI', 'Bea Cukai', '2017-06-04', '2017', 'PC', 'Rakitan', 'BBM/170234 - 0029/A/III/2017', 'BEACUKAI', 'Windows 7', 'Not Ori', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('195', '192.168.40.252', 'BEACUKAI_1', NULL, '2017-11-23', '2017', 'PC', 'Rakitan', 'BBM/170481 - 0056/A/VII/2017', 'BEACUKAI', 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES ('196', '192.168.40.00', 'SGTC', NULL, '2016-12-16', '2016', 'PC', 'Rakitan', ' - ', NULL, 'Windows 7', 'Not Ori', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ip_user` VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for karyawans
-- ----------------------------
DROP TABLE IF EXISTS `karyawans`;
CREATE TABLE `karyawans`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kar_nik` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kar_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kar_phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kar_email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kar_bank` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kar_account` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kar_dept` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kar_active` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of karyawans
-- ----------------------------
INSERT INTO `karyawans` VALUES (1, '10366', 'Haris Rifai', '081464646', 'info@anggunkreasi.com', 'BCA', '225522', 'IT', '1', NULL, NULL);

-- ----------------------------
-- Table structure for komputers
-- ----------------------------
DROP TABLE IF EXISTS `komputers`;
CREATE TABLE `komputers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_pc` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `date_buy` date NULL DEFAULT NULL,
  `date_setup` date NULL DEFAULT NULL,
  `model` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `merek_id` int(10) NULL DEFAULT NULL,
  `dept_id` int(10) NULL DEFAULT NULL,
  `os_build` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remote` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `macaddress` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` int(3) NULL DEFAULT 0,
  `company_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 198 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of komputers
-- ----------------------------
INSERT INTO `komputers` VALUES (1, '192.168.40.2', 'RDP', '1', '2016-01-15', '2016-01-15', 'PC', 7, 1, 'Windows 7', '192.168.40.1', '192.168.40.1', '', 'IT', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (2, '192.168.40.1', 'Kerio', '1', '2017-01-15', '2017-01-15', 'PC', 7, 1, 'Kerio', '20202020202', '22332323', 'harisrifai@anggunkreasi.com', NULL, 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (4, '192.168.40.99', 'X-IT_1', '1', '2019-12-05', '2019-12-05', 'PC', 2, 1, 'Windows 10', NULL, NULL, NULL, 'Arifin', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (5, '192.168.40.10', 'dbakg', '1', '2017-08-21', '2017-08-21', 'Server', 8, 1, 'Windows Server', NULL, NULL, NULL, 'Database', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (6, '192.168.40.9', 'IT', '1', '2018-05-03', '2018-05-03', 'AIO', 1, 1, 'Windows 10', NULL, NULL, NULL, 'Arifin', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (7, '192.168.40.100', 'IT', '1', '2016-12-12', '2016-12-12', 'PC', 8, 1, 'Windows 7', NULL, NULL, NULL, 'RDP', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (8, '192.168.40.83', 'IT', '1', '2020-12-12', '2020-12-12', 'AIO', 1, 1, 'Windows 10', NULL, NULL, NULL, 'IT-Haris', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (9, '192.168.40.50', 'showroom', '1', '2020-12-21', '2020-12-21', 'AIO', 2, 1, 'Windows 10', NULL, NULL, NULL, NULL, 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (10, '192.168.40.200', 'TPB', '1', '2016-06-12', '2016-06-12', 'PC', 7, 1, 'Windows 10', NULL, NULL, NULL, 'SVR-TPB', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (11, '192.168.40.27', 'A-HRD_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Era ', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (12, '192.168.40.22', 'A-HRD_2-PC', '1', '2016-12-18', '2016-12-18', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Sitta', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (13, '192.168.40.23', 'A-HRD_3', '1', '2016-12-18', '2016-12-18', 'PC', 7, 2, 'Windows 10', NULL, NULL, NULL, 'Ajeng', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (14, '192.168.40.31', 'A-HRD_4', '1', '2016-01-01', '2016-01-01', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Rani', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (15, '192.168.40.25', 'A-GA_1', '1', '2016-01-03', '2016-01-03', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Liatri', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (16, '192.168.40.26', 'A_HRD_5', '1', '2017-01-02', '2017-01-02', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Ewin', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (17, '192.168.40.30', 'A-SECURITY', '1', '0000-00-00', '0000-00-00', 'PC', 7, 2, 'Windows 10', NULL, NULL, NULL, 'Teguh', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (18, '192.168.40.28', 'A-HRD_6', '1', '2017-12-22', '2017-12-22', 'AIO', 1, 2, 'Windows 10', NULL, NULL, NULL, 'Vero', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (19, '192.168.40.29', 'A-HRD_7', '1', '2018-02-16', '2018-02-16', 'AIO', 1, 2, 'Windows 10', NULL, NULL, NULL, 'Putri', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (20, '192.168.40.32', 'A-HRD_8', '1', '2018-05-20', '2018-05-20', 'AIO', 1, 2, 'Windows 10', NULL, NULL, NULL, 'Devi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (21, '192.168.40.34', 'F-KLINIK_1', '1', '2020-01-31', '2020-01-31', 'AIO', 2, 2, 'Windows 10', NULL, NULL, NULL, 'Klinik', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (22, '192.168.40.42', 'T-IE_2', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Evelin', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (23, '192.168.40.43', 'T-IE_3', '1', '2017-12-13', '2017-12-13', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dian', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (24, '192.168.40.44', 'T-IE_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Thomas', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (25, '192.168.40.45', 'T-IE_4', '1', '2018-02-16', '2018-02-16', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Cahyo', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (26, '192.168.40.46', 'T-IE_5', '1', '2018-08-28', '2018-08-28', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'dedi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (27, '192.168.40.47', 'T-IE_6', '1', '2018-09-15', '2018-09-15', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'agus', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (28, '192.168.40.48', 'T-IE_7', '1', '2018-11-27', '2018-11-27', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'nurul', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (29, '192.168.40.65', 'O-MEKANIK_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 5, 'Windows 7', NULL, NULL, NULL, 'Mekanik', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (30, '192.168.40.71', 'M-WH_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Sugeng', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (31, '192.168.40.72', 'M-WH_2', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Anah', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (32, '192.168.40.73', 'M-WH_3', '1', '2017-11-03', '2017-11-03', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Vena', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (33, '192.168.40.74', 'M-WH_4', '1', '2017-12-27', '2017-12-27', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Ika', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (34, '192.168.40.75', 'M-WH_5', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Alfi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (35, '192.168.40.76', 'M-WH_6', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Siti', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (36, '192.168.40.77', 'M-WH_7', '1', '2018-10-10', '2018-10-10', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Tanto', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (37, '192.168.40.78', 'M-WH_8', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dipta', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (38, '192.168.40.79', 'M-WH_9', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dea', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (39, '192.168.40.80', 'M-WH_10', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dyah', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (40, '192.168.40.64', 'M-WH_11', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Yuli', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (41, '192.168.40.63', 'M-WH_12', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Ika', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (42, '192.168.40.81', 'Z-COMPONEN_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Vera', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (43, '192.168.40.92', 'B-PROD_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Dian', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (44, '192.168.40.93', 'B-PROD_3', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Karsim', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (45, '192.168.40.54', 'B-PROD_FM', '1', '2018-02-22', '2018-02-22', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Balbir', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (46, '192.168.40.94', 'B-PROD_4', '1', '2019-07-29', '2019-07-29', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'melia', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (47, '192.168.42.186', 'B-PROD_5', '1', '2021-06-21', '2021-06-21', 'AIO', 2, 6, 'Windows 10', NULL, NULL, NULL, 'adm', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (48, '192.168.40.39', 'C-ACC_3', '1', '2016-12-18', '2016-12-18', 'PC', 7, 9, 'Windows 7', NULL, NULL, NULL, 'Arif', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (49, '192.168.40.102', 'C-ACC_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 9, 'Windows 7', NULL, NULL, NULL, 'Ummi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (50, '192.168.40.101', 'C-ACC_2', '1', '2016-12-18', '2016-12-18', 'PC', 7, 9, 'Windows 7', NULL, NULL, NULL, 'Philea', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (51, '192.168.40.104', 'C-ACC_4', '1', '2017-12-22', '2017-12-22', 'AIO', 1, 9, 'Windows 10', NULL, NULL, NULL, 'Dika', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (52, '192.168.40.105', 'C-ACC_5', '1', '2018-07-07', '2018-07-07', 'AIO', 1, 9, 'Windows 10', NULL, NULL, NULL, 'Andaru', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (53, '192.168.40.37', 'C-ACC_6', '1', '2018-10-26', '2018-10-26', 'AIO', 1, 9, 'Windows 10', NULL, NULL, NULL, 'Titi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (54, '192.168.40.38', 'C-ACC_7', '1', '2019-04-08', '2019-04-08', 'AIO', 1, 9, 'Windows 10', NULL, NULL, NULL, 'Taxs', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (55, '192.168.40.40', 'C_ACC_8', '1', '2019-12-31', '2019-12-31', 'AIO', 2, 9, 'Windows 10', NULL, NULL, NULL, 'Fauzie', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (56, '192.168.40.111', 'E-EXIM_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 10, 'Windows 7', NULL, NULL, NULL, 'Heni', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (57, '192.168.40.112', 'E-EXIM_2', '1', '0000-00-00', '0000-00-00', 'PC', 7, 10, 'Windows 7', NULL, NULL, NULL, 'Yeni', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (58, '192.168.40.113', 'E-EXIM_3', '1', '2017-03-11', '2017-03-11', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'PEB', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (59, '192.168.40.226', 'E-EXIM_4', '1', '2017-04-11', '2017-04-11', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'Tanjung', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (60, '192.168.40.115', 'E-EXIM_5', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'Nanda', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (61, '192.168.40.110', 'E-EXIM_6', '1', '2018-03-06', '2018-03-06', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'haris', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (62, '192.168.40.109', 'E-EXIM_7', '1', '2018-03-06', '2018-03-06', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'MD/Milenea', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (63, '192.168.40.108', 'E-EXIM_8', '1', '2018-07-19', '2018-07-19', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'Kenzo', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (64, '192.168.40.107', ' E-EXIM_9', '1', '2019-01-17', '2019-01-17', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'budi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (65, '192.168.40.82', 'E-EXIM_10', '1', '2019-10-29', '2019-10-29', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'yudi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (66, '192.168.40.62', 'E-EXIM_11', '1', '2021-01-14', '2021-01-14', 'AIO', 2, 10, 'Windows 10', NULL, NULL, NULL, 'vika', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (67, '192.168.40.116', 'Totok', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_1', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (68, '192.168.40.117', 'April', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_2', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (69, '192.168.40.118', 'faulin', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_3', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (70, '192.168.40.119', 'Kendi', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_4', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (71, '192.168.40.120', 'Geovani', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_5', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (72, '192.168.40.122', 'Dyaning', '1', '2018-03-01', '2018-03-01', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_6', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (73, '192.168.40.123', 'Arya', '1', '2018-03-01', '2018-03-01', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_7', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (74, '192.168.40.124', 'Rinda', '1', '2018-03-01', '2018-03-01', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_8', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (75, '192.168.40.149', 'Amy', '1', '2018-05-03', '2018-05-03', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_10', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (76, '192.168.40.150', 'Dicky', '1', '2018-05-07', '2018-05-07', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_11', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (77, '192.168.40.151', 'Oca', '1', '2018-05-15', '2018-05-15', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_12', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (78, '192.168.40.152', 'Atta', '1', '2018-06-04', '2018-06-04', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'MD_Atta', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (79, '192.168.40.153', 'Agnes', '1', '2018-06-05', '2018-06-05', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_13', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (80, '192.168.40.154', 'Yanti', '1', '2018-07-07', '2018-07-07', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_14', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (81, '192.168.40.155', 'Deris', '1', '2018-07-19', '2018-07-19', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_15', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (82, '192.168.40.156', 'ex Darsono', '1', '2018-08-03', '2018-08-03', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_16', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (83, '192.168.40.157', 'Trias', '1', '2019-04-29', '2019-04-29', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_17', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (84, '192.168.40.158', 'Herna', '1', '2019-07-29', '2019-07-29', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_18', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (85, '192.168.40.70', 'Gema', '1', '2019-08-08', '2019-08-08', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_19', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (86, '192.168.40.247', 'Tri', '1', '2019-08-12', '2019-08-12', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_20', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (87, '192.168.40.227', 'Azura', '1', '2020-01-29', '2020-01-29', 'AIO', 2, 11, 'Windows 10', NULL, NULL, NULL, 'D-DMD_21', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (88, '192.168.40.122', 'DYANING', '1', '2020-12-18', '2020-12-18', 'AIO', 2, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_22', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (89, '192.168.41.249', 'Ella', '1', '2021-02-15', '2021-02-15', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_23', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (90, '192.168.41.71', 'FRANDO', '1', '2021-06-21', '2021-06-21', 'AIO', 2, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_24', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (91, '192.168.41.210', 'Savira', '1', '2021-06-21', '2021-06-21', 'AIO', 2, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD-25', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (92, '192.168.42.0', 'Maike', '1', '2021-08-04', '2021-08-04', 'AIO', 1, 11, NULL, NULL, NULL, NULL, 'D-MD-26', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (93, '192.168.42.0', 'Marketing', '1', '2018-06-04', '2018-06-04', 'AIO', 1, 11, NULL, NULL, NULL, NULL, 'Dita', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (94, '192.168.42.0', 'Marketing', '1', '2018-06-04', '2018-06-04', 'AIO', 1, 11, NULL, NULL, NULL, NULL, 'Septiana', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (95, '192.168.40.86', 'H-MARKETING_2', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Ana', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (96, '192.168.40.85', 'H-MARKETING_1', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Dita', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (97, '192.168.40.87', 'H-MARKETING_3', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Nana ', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (98, '192.168.40.88', 'H-MARKETING_4', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Fenty', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (99, '192.168.40.41', 'Abhisek', '1', '2018-01-12', '2018-01-12', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Abhisek', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (100, '192.168.41.88', 'H-MARKETING_5', '1', '0000-00-00', '0000-00-00', 'AIO', 2, 12, 'Windows 10', NULL, NULL, NULL, 'Rina', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (101, '192.168.40.125', 'G-DESIGN_1/D-MD_22', '1', '2017-08-15', '2017-08-15', 'NUC', 9, 13, 'Windows 10', NULL, NULL, NULL, 'Nova', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (102, '192.168.40.126', 'G-DESAIN_1', '1', '2017-08-19', '2017-08-19', 'AIO', 1, 13, 'Windows 10', NULL, NULL, NULL, 'VERO', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (103, '192.168.40.127', 'G-DESAIN_2', '1', '2017-12-22', '2017-12-22', 'AIO', 1, 13, 'Windows 10', NULL, NULL, NULL, 'Yunita', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (104, '192.168.40.128', 'G-DESAIN_5', '1', '0000-00-00', '0000-00-00', 'PC', 7, 13, 'Windows 7', NULL, NULL, NULL, 'Nugroho', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (105, '192.168.40.129', 'G-DESAIN_3', '1', '0000-00-00', '0000-00-00', NULL, 1, 13, NULL, NULL, NULL, NULL, 'Patty', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (106, '192.168.40.129', 'G-DESAIN_3', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 13, NULL, NULL, NULL, NULL, 'Patty', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (107, '192.168.40.229', 'G-DESAIN_4', '1', '2020-02-24', '2020-02-24', 'AIO', 2, 13, 'Windows 10', NULL, NULL, NULL, 'nova', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (108, '192.168.40.239', 'G-DESAIN_6', '1', '0000-00-00', '0000-00-00', 'PC', 2, 13, 'Windows 10', NULL, NULL, NULL, '3D', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (109, '192.168.42.239', 'G-DESAIN_7', '1', '0000-00-00', '0000-00-00', 'PC', 2, 13, 'Windows 10', NULL, NULL, NULL, '3D', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (110, '192.168.40.130', 'P-PURCHASE_1', '1', '2017-07-12', '2017-07-12', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Wiji', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (111, '192.168.40.137', 'Purchase', '1', '2018-01-03', '2018-01-03', 'PC', 7, 15, 'Windows 7', NULL, NULL, NULL, 'Much Fajar', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (112, '192.168.40.139', 'P-PURCHASE_2', '1', '2018-05-17', '2018-05-17', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Haryo', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (113, '192.168.40.140', 'P-PURCHASE_3', '1', '2018-09-18', '2018-09-18', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Prajakti', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (114, '192.168.40.138', 'P-PURCHASE_4', '1', '2018-09-18', '2018-09-18', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Diksan', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (115, '192.168.40.136', 'P-PURCHASE_5', '1', '2018-11-19', '2018-11-19', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'luluc', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (116, '192.168.40.49', 'P-PURCHASE_6', '1', '2018-11-19', '2018-11-19', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Indri', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (117, '192.168.40.131', 'Y-CUTTING_1', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Dewi ', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (118, '192.168.40.132', 'Y-CUTTING_2', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Aji', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (119, '192.168.40.133', 'Y-CUTTING_3', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Hari ', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (120, '192.168.40.134', 'Y-CUTTING_4', '1', '2018-05-24', '2018-05-24', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Bundle', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (121, '192.168.40.135', 'Y-CUTTING_5', '1', '2018-08-03', '2018-08-03', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dina', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (122, '192.168.42.13', 'LASER', '1', '2020-10-21', '2020-10-21', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Venda', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (123, '192.168.40.141', 'K-QC_1', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Niken', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (124, '192.168.40.142', 'K-QC_2', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Temi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (125, '192.168.40.143', 'K-QC_3', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Wanto', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (126, '192.168.40.144', 'K-QC_4', '1', '2017-09-08', '2017-09-08', 'NUC', 9, 6, 'Windows 7', NULL, NULL, NULL, 'Lilis', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (127, '192.168.40.145', 'K-QCBUYER_1', '1', '2017-12-19', '2017-12-19', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Tari', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (128, '192.168.40.148', 'LAB', '1', '2018-03-02', '2018-03-02', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Shena', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (129, '192.168.40.147', 'K-QC_5', '1', '2018-09-14', '2018-09-14', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'adm', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (130, '192.168.40.96', 'K-QC_6', '1', '2018-10-10', '2018-10-10', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Niken', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (131, '192.168.40.98', 'K-QC_8', '1', '2018-10-15', '2018-10-15', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Wiwin', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (132, '192.168.40.97', 'K-QC_9', '1', '2018-11-19', '2018-11-19', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Fabric', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (133, '192.168.41.112', 'K-QC_10', '1', '2021-01-14', '2021-01-14', 'AIO', 2, 6, 'Windows 10', NULL, NULL, NULL, 'sot', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (134, '192.168.40.243', 'K-IQT_1', '1', '2021-06-17', '2021-06-17', 'AIO', 2, 6, 'Windows 10', NULL, NULL, NULL, 'Sigit', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (135, '192.168.40.90', 'B-PROD_2', '1', '2016-05-04', '2016-05-04', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Jonno', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (136, '192.168.40.162', 'W-PACKING_2', '1', '2016-05-04', '2016-05-04', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Nur', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (137, '192.168.40.163', 'W-PACKING_3', '1', '2017-12-27', '2017-12-27', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Brina', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (138, '192.168.40.164', 'W-PACKING_4', '1', '2018-03-02', '2018-03-02', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Rista', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (139, '192.168.40.160', 'W-PACKING_5', '1', '2018-05-03', '2018-05-03', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Novia', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (140, '192.168.40.159', 'W-PACKING_7', '1', '2019-06-29', '2019-06-29', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'wulan', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (141, '192,168,40,165', 'W-PACKING_8', '1', '2019-07-29', '2019-07-29', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'nur', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (142, '192.168.40.67', 'W-WASHING_1', '1', '2019-07-29', '2019-07-29', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Aan', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (143, '192.168.40.16', 'W-BARCODE', '1', '2020-10-12', '2020-10-12', 'PC', 7, 6, 'Windows 10', NULL, NULL, NULL, 'barcode', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (144, '192.168.40.190', 'I-SAMPLE_2', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Rizky', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (145, '192.168.40.188', 'I-POLA_5', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Marino ', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (146, '192.168.40.187', 'I-PATTERN_1', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, NULL, 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (147, '192.168.40.186', 'ADMSAMPLE', '1', '2017-11-12', '2017-11-12', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Lora', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (148, '192.168.40.184', 'I-MARKER_1', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Tugini', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (149, '192.168.40.182', 'I-CAD_2', '1', '0000-00-00', '0000-00-00', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Tri', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (150, '192.168.40.181', 'I-CAD_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Restu', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (151, '192.168.40.180', 'I-ADMPATTERN', '1', '2017-05-06', '2017-05-06', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Erna', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (152, '192.168.40.189', 'I-POLA_4', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Windy', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (153, '192.168.40.177', 'Marker', '1', '0000-00-00', '0000-00-00', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Yuli', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (154, '192.168.40.176', 'I-CAD_3', '1', '2017-10-11', '2017-10-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Singgih', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (155, '192.168.40.175', 'I-SAMPLE_4', '1', '2017-10-11', '2017-10-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Budi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (156, '192.168.40.174', 'I-SAMPLE_3', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Indar', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (157, '192.168.40.173', 'I-POLA_3', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Bayu', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (158, '192.168.40.172', 'I-POLA_2', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Heri', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (159, '192.168.40.171', 'I-POLA_1', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Irwan', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (160, '192.168.40.170', 'I-Marker_2', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'nurw', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (161, '192.168.40.169', 'I-SAMPLE_1', '1', '2018-02-22', '2018-02-22', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Dendi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (162, '192.168.40.168', 'I-SAMPLE_15', '1', '2017-03-11', '2017-03-11', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Dini', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (163, '192.168.40.206', 'I-ADMSAMPLE_2', '1', '2018-03-13', '2018-03-13', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Riska', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (164, '192.168.40.146', 'K-QA_1', '1', '2018-03-11', '2018-03-11', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Imam', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (165, '192.168.40.167', 'I-SAMPLE_5', '1', '2018-06-04', '2018-06-04', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Fitri', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (166, '192.168.40.166', 'I-SAMPLE_6', '1', '2018-06-04', '2018-06-04', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Udin', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (167, '192.168.40.201', 'I-SAMPLE_7', '1', '2018-08-03', '2018-08-03', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'arfi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (168, '192.168.40.202', 'I-SAMPLE_8', '1', '2018-08-03', '2018-08-03', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Nurul', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (169, '192.168.40.203', 'I-SAMPLE_9', '1', '2018-08-07', '2018-08-07', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'heru', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (170, '192.168.40.204', 'I-SAMPLE_10', '1', '2018-08-07', '2018-08-07', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Rita', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (171, '192.168.40.205', 'I-SAMPLE_11', '1', '2018-09-26', '2018-09-26', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Alfi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (172, '192.168.40.208', 'I-SAMPLE_12', '1', '2018-10-03', '2018-10-03', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Puput', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (173, '192.168.40.207', 'I-SAMPLE_13', '1', '2018-11-19', '2018-11-19', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'cutting', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (174, '192.168.40.209', 'I-SAMPLE_14', '1', '2019-06-10', '2019-06-10', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'satrio', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (175, '192.168.40.178', 'I-SAMPLE_15', '1', '2019-06-10', '2019-06-10', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Elis', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (176, '192.168.40.', 'I-SAMPLE_16', '1', '2019-08-21', '2019-08-21', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Alfa', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (177, '192.168.40.235', 'I-SAMPLE_17', '1', '2019-10-01', '2019-10-01', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'tessa', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (178, '192.168.40.223', 'I-SAMPLE_18', '1', '2020-01-31', '2020-01-31', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'P putut', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (179, '192.168.40.', 'I-SAMPLE_19', '1', '2020-01-31', '2020-01-31', 'PC', 7, 17, 'Windows 10', NULL, NULL, NULL, 'Retno', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (180, '192.168.40.231', 'I-SAMPLE_20', '1', '2021-06-30', '2021-06-30', 'PC', 2, 17, 'Windows 10', NULL, NULL, NULL, 'Nur', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (181, '192.168.40.191', 'S-PPIC_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Yani', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (182, '192.168.40.192', 'S-PPIC_2/', '1', '0000-00-00', '0000-00-00', NULL, 1, 6, NULL, NULL, NULL, NULL, 'Ratna ', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (183, '192.168.40.225', 'S-PPIC_3/K-QC_10', '1', '2018-03-11', '2018-03-11', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'siwi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (184, '192.168.40.194', 'S-PPIC_4', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Nurul', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (185, '192.168.40.195', 'S-PPIC_5/QC', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Isti', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (186, '192.168.40.161', 'W-PACKING_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Ardi ', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (187, '192.168.40.196', 'S-PPIC_6', '1', '2018-06-05', '2018-06-05', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Rina', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (188, '192.168.40.197 ', 'S-PPIC_7', '1', '2018-10-22', '2018-10-22', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Hepi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (189, '192.168.40.198', 'S-PPIC_8', '1', '2018-10-22', '2018-10-22', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Mifta', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (190, '192.168.40.199', 'S-PPIC_9', '1', '2018-10-22', '2018-10-22', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Arvi', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (191, '192.168.40.66', 'S-PPIC_10', '1', '2018-12-16', '2018-12-16', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Iin', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (192, '192.168.40.192', 'N-PLANNING_1', '1', '2019-10-01', '2019-10-01', 'PC', 7, 6, NULL, NULL, NULL, NULL, 'Ratna ', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (193, '192.168.40.198', 'N-PLANNING_2', '1', '2019-10-01', '2019-10-01', 'PC', 7, 6, 'Windows 10', NULL, NULL, NULL, 'Wury', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (194, '192.168.40.61', 'O-MTN_1', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Adm. Mtn', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (195, '192.168.40.251', 'BEA_CUKAI', '1', '2017-06-04', '2017-06-04', 'PC', 7, 23, 'Windows 7', NULL, NULL, NULL, 'Bea Cukai', 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (196, '192.168.40.252', 'BEACUKAI_1', '1', '2017-11-23', '2017-11-23', 'PC', 7, 23, 'Windows 7', NULL, NULL, NULL, NULL, 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers` VALUES (197, '192.168.40.00', 'SGTC', '1', '2016-12-16', '2016-12-16', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, NULL, 0, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');

-- ----------------------------
-- Table structure for komputers_copy1
-- ----------------------------
DROP TABLE IF EXISTS `komputers_copy1`;
CREATE TABLE `komputers_copy1`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_pc` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comp` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `date_buy` date NULL DEFAULT NULL,
  `date_setup` date NULL DEFAULT NULL,
  `model` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `merek_id` int(10) NULL DEFAULT NULL,
  `dept_id` int(10) NULL DEFAULT NULL,
  `os_build` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remote` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `macaddress` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `active` int(3) NULL DEFAULT NULL,
  `company_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1028 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of komputers_copy1
-- ----------------------------
INSERT INTO `komputers_copy1` VALUES (1, '192.168.40.2', 'RDP', '1', '2016-01-15', '2016-01-15', 'PC', 7, 1, 'Windows 7', '192.168.40.1', '192.168.40.1', '', 'IT', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (2, '192.168.40.1', 'Kerio', '1', '2017-01-15', '2017-01-15', 'PC', 7, 1, 'Kerio', '20202020202', '22332323', 'harisrifai@anggunkreasi.com', NULL, 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (4, '192.168.40.99', 'X-IT_1', '1', '2019-12-05', '2019-12-05', 'PC', 2, 1, 'Windows 10', NULL, NULL, NULL, 'Arifin', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (5, '192.168.40.10', 'dbakg', '1', '2017-08-21', '2017-08-21', 'Server', 8, 1, 'Windows Server', NULL, NULL, NULL, 'Database', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (6, '192.168.40.9', 'IT', '1', '2018-05-03', '2018-05-03', 'AIO', 1, 1, 'Windows 10', NULL, NULL, NULL, 'Arifin', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (7, '192.168.40.100', 'IT', '1', '2016-12-12', '2016-12-12', 'PC', 8, 1, 'Windows 7', NULL, NULL, NULL, 'RDP', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (8, '192.168.40.83', 'IT', '1', '2020-12-12', '2020-12-12', 'AIO', 1, 1, 'Windows 10', NULL, NULL, NULL, 'IT-Haris', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (9, '192.168.40.50', 'showroom', '1', '2020-12-21', '2020-12-21', 'AIO', 2, 1, 'Windows 10', NULL, NULL, NULL, NULL, 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (10, '192.168.40.200', 'TPB', '1', '2016-06-12', '2016-06-12', 'PC', 7, 1, 'Windows 10', NULL, NULL, NULL, 'SVR-TPB', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (11, '192.168.40.27', 'A-HRD_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Era ', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (12, '192.168.40.22', 'A-HRD_2-PC', '1', '2016-12-18', '2016-12-18', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Sitta', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (13, '192.168.40.23', 'A-HRD_3', '1', '2016-12-18', '2016-12-18', 'PC', 7, 2, 'Windows 10', NULL, NULL, NULL, 'Ajeng', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (14, '192.168.40.31', 'A-HRD_4', '1', '2016-01-01', '2016-01-01', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Rani', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (15, '192.168.40.25', 'A-GA_1', '1', '2016-01-03', '2016-01-03', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Liatri', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (16, '192.168.40.26', 'A_HRD_5', '1', '2017-01-02', '2017-01-02', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, 'Ewin', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (17, '192.168.40.30', 'A-SECURITY', '1', '0000-00-00', '0000-00-00', 'PC', 7, 2, 'Windows 10', NULL, NULL, NULL, 'Teguh', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (18, '192.168.40.28', 'A-HRD_6', '1', '2017-12-22', '2017-12-22', 'AIO', 1, 2, 'Windows 10', NULL, NULL, NULL, 'Vero', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (19, '192.168.40.29', 'A-HRD_7', '1', '2018-02-16', '2018-02-16', 'AIO', 1, 2, 'Windows 10', NULL, NULL, NULL, 'Putri', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (20, '192.168.40.32', 'A-HRD_8', '1', '2018-05-20', '2018-05-20', 'AIO', 1, 2, 'Windows 10', NULL, NULL, NULL, 'Devi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (21, '192.168.40.34', 'F-KLINIK_1', '1', '2020-01-31', '2020-01-31', 'AIO', 2, 2, 'Windows 10', NULL, NULL, NULL, 'Klinik', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (22, '192.168.40.42', 'T-IE_2', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Evelin', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (23, '192.168.40.43', 'T-IE_3', '1', '2017-12-13', '2017-12-13', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dian', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (24, '192.168.40.44', 'T-IE_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Thomas', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (25, '192.168.40.45', 'T-IE_4', '1', '2018-02-16', '2018-02-16', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Cahyo', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (26, '192.168.40.46', 'T-IE_5', '1', '2018-08-28', '2018-08-28', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'dedi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (27, '192.168.40.47', 'T-IE_6', '1', '2018-09-15', '2018-09-15', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'agus', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (28, '192.168.40.48', 'T-IE_7', '1', '2018-11-27', '2018-11-27', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'nurul', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (29, '192.168.40.65', 'O-MEKANIK_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 5, 'Windows 7', NULL, NULL, NULL, 'Mekanik', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (30, '192.168.40.71', 'M-WH_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Sugeng', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (31, '192.168.40.72', 'M-WH_2', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Anah', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (32, '192.168.40.73', 'M-WH_3', '1', '2017-11-03', '2017-11-03', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Vena', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (33, '192.168.40.74', 'M-WH_4', '1', '2017-12-27', '2017-12-27', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Ika', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (34, '192.168.40.75', 'M-WH_5', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Alfi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (35, '192.168.40.76', 'M-WH_6', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Siti', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (36, '192.168.40.77', 'M-WH_7', '1', '2018-10-10', '2018-10-10', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Tanto', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (37, '192.168.40.78', 'M-WH_8', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dipta', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (38, '192.168.40.79', 'M-WH_9', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dea', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (39, '192.168.40.80', 'M-WH_10', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dyah', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (40, '192.168.40.64', 'M-WH_11', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Yuli', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (41, '192.168.40.63', 'M-WH_12', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Ika', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (42, '192.168.40.81', 'Z-COMPONEN_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Vera', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (43, '192.168.40.92', 'B-PROD_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Dian', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (44, '192.168.40.93', 'B-PROD_3', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Karsim', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (45, '192.168.40.54', 'B-PROD_FM', '1', '2018-02-22', '2018-02-22', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Balbir', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (46, '192.168.40.94', 'B-PROD_4', '1', '2019-07-29', '2019-07-29', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'melia', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (47, '192.168.42.186', 'B-PROD_5', '1', '2021-06-21', '2021-06-21', 'AIO', 2, 6, 'Windows 10', NULL, NULL, NULL, 'adm', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (48, '192.168.40.39', 'C-ACC_3', '1', '2016-12-18', '2016-12-18', 'PC', 7, 9, 'Windows 7', NULL, NULL, NULL, 'Arif', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (49, '192.168.40.102', 'C-ACC_1', '1', '2016-12-18', '2016-12-18', 'PC', 7, 9, 'Windows 7', NULL, NULL, NULL, 'Ummi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (50, '192.168.40.101', 'C-ACC_2', '1', '2016-12-18', '2016-12-18', 'PC', 7, 9, 'Windows 7', NULL, NULL, NULL, 'Philea', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (51, '192.168.40.104', 'C-ACC_4', '1', '2017-12-22', '2017-12-22', 'AIO', 1, 9, 'Windows 10', NULL, NULL, NULL, 'Dika', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (52, '192.168.40.105', 'C-ACC_5', '1', '2018-07-07', '2018-07-07', 'AIO', 1, 9, 'Windows 10', NULL, NULL, NULL, 'Andaru', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (53, '192.168.40.37', 'C-ACC_6', '1', '2018-10-26', '2018-10-26', 'AIO', 1, 9, 'Windows 10', NULL, NULL, NULL, 'Titi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (54, '192.168.40.38', 'C-ACC_7', '1', '2019-04-08', '2019-04-08', 'AIO', 1, 9, 'Windows 10', NULL, NULL, NULL, 'Taxs', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (55, '192.168.40.40', 'C_ACC_8', '1', '2019-12-31', '2019-12-31', 'AIO', 2, 9, 'Windows 10', NULL, NULL, NULL, 'Fauzie', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (56, '192.168.40.111', 'E-EXIM_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 10, 'Windows 7', NULL, NULL, NULL, 'Heni', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (57, '192.168.40.112', 'E-EXIM_2', '1', '0000-00-00', '0000-00-00', 'PC', 7, 10, 'Windows 7', NULL, NULL, NULL, 'Yeni', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (58, '192.168.40.113', 'E-EXIM_3', '1', '2017-03-11', '2017-03-11', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'PEB', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (59, '192.168.40.226', 'E-EXIM_4', '1', '2017-04-11', '2017-04-11', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'Tanjung', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (60, '192.168.40.115', 'E-EXIM_5', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'Nanda', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (61, '192.168.40.110', 'E-EXIM_6', '1', '2018-03-06', '2018-03-06', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'haris', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (62, '192.168.40.109', 'E-EXIM_7', '1', '2018-03-06', '2018-03-06', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'MD/Milenea', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (63, '192.168.40.108', 'E-EXIM_8', '1', '2018-07-19', '2018-07-19', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'Kenzo', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (64, '192.168.40.107', ' E-EXIM_9', '1', '2019-01-17', '2019-01-17', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'budi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (65, '192.168.40.82', 'E-EXIM_10', '1', '2019-10-29', '2019-10-29', 'AIO', 1, 10, 'Windows 10', NULL, NULL, NULL, 'yudi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (66, '192.168.40.62', 'E-EXIM_11', '1', '2021-01-14', '2021-01-14', 'AIO', 2, 10, 'Windows 10', NULL, NULL, NULL, 'vika', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (67, '192.168.40.116', 'Totok', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_1', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (68, '192.168.40.117', 'April', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_2', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (69, '192.168.40.118', 'faulin', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_3', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (70, '192.168.40.119', 'Kendi', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_4', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (71, '192.168.40.120', 'Geovani', '1', '2017-11-09', '2017-11-09', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_5', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (72, '192.168.40.122', 'Dyaning', '1', '2018-03-01', '2018-03-01', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_6', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (73, '192.168.40.123', 'Arya', '1', '2018-03-01', '2018-03-01', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_7', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (74, '192.168.40.124', 'Rinda', '1', '2018-03-01', '2018-03-01', 'PC', 7, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_8', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (75, '192.168.40.149', 'Amy', '1', '2018-05-03', '2018-05-03', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_10', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (76, '192.168.40.150', 'Dicky', '1', '2018-05-07', '2018-05-07', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_11', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (77, '192.168.40.151', 'Oca', '1', '2018-05-15', '2018-05-15', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_12', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (78, '192.168.40.152', 'Atta', '1', '2018-06-04', '2018-06-04', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'MD_Atta', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (79, '192.168.40.153', 'Agnes', '1', '2018-06-05', '2018-06-05', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_13', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (80, '192.168.40.154', 'Yanti', '1', '2018-07-07', '2018-07-07', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_14', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (81, '192.168.40.155', 'Deris', '1', '2018-07-19', '2018-07-19', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_15', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (82, '192.168.40.156', 'ex Darsono', '1', '2018-08-03', '2018-08-03', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_16', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (83, '192.168.40.157', 'Trias', '1', '2019-04-29', '2019-04-29', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_17', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (84, '192.168.40.158', 'Herna', '1', '2019-07-29', '2019-07-29', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_18', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (85, '192.168.40.70', 'Gema', '1', '2019-08-08', '2019-08-08', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_19', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (86, '192.168.40.247', 'Tri', '1', '2019-08-12', '2019-08-12', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_20', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (87, '192.168.40.227', 'Azura', '1', '2020-01-29', '2020-01-29', 'AIO', 2, 11, 'Windows 10', NULL, NULL, NULL, 'D-DMD_21', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (88, '192.168.40.122', 'DYANING', '1', '2020-12-18', '2020-12-18', 'AIO', 2, 11, 'Windows 7', NULL, NULL, NULL, 'D-MD_22', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (89, '192.168.41.249', 'Ella', '1', '2021-02-15', '2021-02-15', 'AIO', 1, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_23', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (90, '192.168.41.71', 'FRANDO', '1', '2021-06-21', '2021-06-21', 'AIO', 2, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD_24', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (91, '192.168.41.210', 'Savira', '1', '2021-06-21', '2021-06-21', 'AIO', 2, 11, 'Windows 10', NULL, NULL, NULL, 'D-MD-25', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (92, '192.168.42.0', 'Maike', '1', '2021-08-04', '2021-08-04', 'AIO', 1, 11, NULL, NULL, NULL, NULL, 'D-MD-26', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (93, '192.168.42.0', 'Marketing', '1', '2018-06-04', '2018-06-04', 'AIO', 1, 11, NULL, NULL, NULL, NULL, 'Dita', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (94, '192.168.42.0', 'Marketing', '1', '2018-06-04', '2018-06-04', 'AIO', 1, 11, NULL, NULL, NULL, NULL, 'Septiana', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (95, '192.168.40.86', 'H-MARKETING_2', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Ana', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (96, '192.168.40.85', 'H-MARKETING_1', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Dita', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (97, '192.168.40.87', 'H-MARKETING_3', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Nana ', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (98, '192.168.40.88', 'H-MARKETING_4', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Fenty', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (99, '192.168.40.41', 'Abhisek', '1', '2018-01-12', '2018-01-12', 'AIO', 1, 12, 'Windows 10', NULL, NULL, NULL, 'Abhisek', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (100, '192.168.41.88', 'H-MARKETING_5', '1', '0000-00-00', '0000-00-00', 'AIO', 2, 12, 'Windows 10', NULL, NULL, NULL, 'Rina', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (101, '192.168.40.125', 'G-DESIGN_1/D-MD_22', '1', '2017-08-15', '2017-08-15', 'NUC', 9, 13, 'Windows 10', NULL, NULL, NULL, 'Nova', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (102, '192.168.40.126', 'G-DESAIN_1', '1', '2017-08-19', '2017-08-19', 'AIO', 1, 13, 'Windows 10', NULL, NULL, NULL, 'VERO', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (103, '192.168.40.127', 'G-DESAIN_2', '1', '2017-12-22', '2017-12-22', 'AIO', 1, 13, 'Windows 10', NULL, NULL, NULL, 'Yunita', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (104, '192.168.40.128', 'G-DESAIN_5', '1', '0000-00-00', '0000-00-00', 'PC', 7, 13, 'Windows 7', NULL, NULL, NULL, 'Nugroho', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (105, '192.168.40.129', 'G-DESAIN_3', '1', '0000-00-00', '0000-00-00', NULL, 1, 13, NULL, NULL, NULL, NULL, 'Patty', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (106, '192.168.40.129', 'G-DESAIN_3', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 13, NULL, NULL, NULL, NULL, 'Patty', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (107, '192.168.40.229', 'G-DESAIN_4', '1', '2020-02-24', '2020-02-24', 'AIO', 2, 13, 'Windows 10', NULL, NULL, NULL, 'nova', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (108, '192.168.40.239', 'G-DESAIN_6', '1', '0000-00-00', '0000-00-00', 'PC', 2, 13, 'Windows 10', NULL, NULL, NULL, '3D', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (109, '192.168.42.239', 'G-DESAIN_7', '1', '0000-00-00', '0000-00-00', 'PC', 2, 13, 'Windows 10', NULL, NULL, NULL, '3D', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (110, '192.168.40.130', 'P-PURCHASE_1', '1', '2017-07-12', '2017-07-12', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Wiji', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (111, '192.168.40.137', 'Purchase', '1', '2018-01-03', '2018-01-03', 'PC', 7, 15, 'Windows 7', NULL, NULL, NULL, 'Much Fajar', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (112, '192.168.40.139', 'P-PURCHASE_2', '1', '2018-05-17', '2018-05-17', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Haryo', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (113, '192.168.40.140', 'P-PURCHASE_3', '1', '2018-09-18', '2018-09-18', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Prajakti', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (114, '192.168.40.138', 'P-PURCHASE_4', '1', '2018-09-18', '2018-09-18', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Diksan', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (115, '192.168.40.136', 'P-PURCHASE_5', '1', '2018-11-19', '2018-11-19', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'luluc', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (116, '192.168.40.49', 'P-PURCHASE_6', '1', '2018-11-19', '2018-11-19', 'AIO', 1, 15, 'Windows 10', NULL, NULL, NULL, 'Indri', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (117, '192.168.40.131', 'Y-CUTTING_1', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Dewi ', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (118, '192.168.40.132', 'Y-CUTTING_2', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Aji', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (119, '192.168.40.133', 'Y-CUTTING_3', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Hari ', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (120, '192.168.40.134', 'Y-CUTTING_4', '1', '2018-05-24', '2018-05-24', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Bundle', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (121, '192.168.40.135', 'Y-CUTTING_5', '1', '2018-08-03', '2018-08-03', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Dina', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (122, '192.168.42.13', 'LASER', '1', '2020-10-21', '2020-10-21', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Venda', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (123, '192.168.40.141', 'K-QC_1', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Niken', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (124, '192.168.40.142', 'K-QC_2', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Temi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (125, '192.168.40.143', 'K-QC_3', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Wanto', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (126, '192.168.40.144', 'K-QC_4', '1', '2017-09-08', '2017-09-08', 'NUC', 9, 6, 'Windows 7', NULL, NULL, NULL, 'Lilis', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (127, '192.168.40.145', 'K-QCBUYER_1', '1', '2017-12-19', '2017-12-19', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Tari', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (128, '192.168.40.148', 'LAB', '1', '2018-03-02', '2018-03-02', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Shena', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (129, '192.168.40.147', 'K-QC_5', '1', '2018-09-14', '2018-09-14', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'adm', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (130, '192.168.40.96', 'K-QC_6', '1', '2018-10-10', '2018-10-10', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Niken', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (131, '192.168.40.98', 'K-QC_8', '1', '2018-10-15', '2018-10-15', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Wiwin', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (132, '192.168.40.97', 'K-QC_9', '1', '2018-11-19', '2018-11-19', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Fabric', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (133, '192.168.41.112', 'K-QC_10', '1', '2021-01-14', '2021-01-14', 'AIO', 2, 6, 'Windows 10', NULL, NULL, NULL, 'sot', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (134, '192.168.40.243', 'K-IQT_1', '1', '2021-06-17', '2021-06-17', 'AIO', 2, 6, 'Windows 10', NULL, NULL, NULL, 'Sigit', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (135, '192.168.40.90', 'B-PROD_2', '1', '2016-05-04', '2016-05-04', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Jonno', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (136, '192.168.40.162', 'W-PACKING_2', '1', '2016-05-04', '2016-05-04', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Nur', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (137, '192.168.40.163', 'W-PACKING_3', '1', '2017-12-27', '2017-12-27', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Brina', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (138, '192.168.40.164', 'W-PACKING_4', '1', '2018-03-02', '2018-03-02', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Rista', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (139, '192.168.40.160', 'W-PACKING_5', '1', '2018-05-03', '2018-05-03', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Novia', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (140, '192.168.40.159', 'W-PACKING_7', '1', '2019-06-29', '2019-06-29', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'wulan', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (141, '192,168,40,165', 'W-PACKING_8', '1', '2019-07-29', '2019-07-29', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'nur', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (142, '192.168.40.67', 'W-WASHING_1', '1', '2019-07-29', '2019-07-29', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Aan', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (143, '192.168.40.16', 'W-BARCODE', '1', '2020-10-12', '2020-10-12', 'PC', 7, 6, 'Windows 10', NULL, NULL, NULL, 'barcode', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (144, '192.168.40.190', 'I-SAMPLE_2', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Rizky', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (145, '192.168.40.188', 'I-POLA_5', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Marino ', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (146, '192.168.40.187', 'I-PATTERN_1', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, NULL, 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (147, '192.168.40.186', 'ADMSAMPLE', '1', '2017-11-12', '2017-11-12', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Lora', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (148, '192.168.40.184', 'I-MARKER_1', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Tugini', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (149, '192.168.40.182', 'I-CAD_2', '1', '0000-00-00', '0000-00-00', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Tri', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (150, '192.168.40.181', 'I-CAD_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Restu', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (151, '192.168.40.180', 'I-ADMPATTERN', '1', '2017-05-06', '2017-05-06', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Erna', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (152, '192.168.40.189', 'I-POLA_4', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Windy', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (153, '192.168.40.177', 'Marker', '1', '0000-00-00', '0000-00-00', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Yuli', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (154, '192.168.40.176', 'I-CAD_3', '1', '2017-10-11', '2017-10-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Singgih', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (155, '192.168.40.175', 'I-SAMPLE_4', '1', '2017-10-11', '2017-10-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Budi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (156, '192.168.40.174', 'I-SAMPLE_3', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Indar', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (157, '192.168.40.173', 'I-POLA_3', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Bayu', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (158, '192.168.40.172', 'I-POLA_2', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Heri', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (159, '192.168.40.171', 'I-POLA_1', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Irwan', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (160, '192.168.40.170', 'I-Marker_2', '1', '2017-03-11', '2017-03-11', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'nurw', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (161, '192.168.40.169', 'I-SAMPLE_1', '1', '2018-02-22', '2018-02-22', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Dendi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (162, '192.168.40.168', 'I-SAMPLE_15', '1', '2017-03-11', '2017-03-11', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Dini', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (163, '192.168.40.206', 'I-ADMSAMPLE_2', '1', '2018-03-13', '2018-03-13', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Riska', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (164, '192.168.40.146', 'K-QA_1', '1', '2018-03-11', '2018-03-11', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Imam', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (165, '192.168.40.167', 'I-SAMPLE_5', '1', '2018-06-04', '2018-06-04', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Fitri', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (166, '192.168.40.166', 'I-SAMPLE_6', '1', '2018-06-04', '2018-06-04', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Udin', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (167, '192.168.40.201', 'I-SAMPLE_7', '1', '2018-08-03', '2018-08-03', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'arfi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (168, '192.168.40.202', 'I-SAMPLE_8', '1', '2018-08-03', '2018-08-03', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Nurul', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (169, '192.168.40.203', 'I-SAMPLE_9', '1', '2018-08-07', '2018-08-07', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'heru', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (170, '192.168.40.204', 'I-SAMPLE_10', '1', '2018-08-07', '2018-08-07', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Rita', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (171, '192.168.40.205', 'I-SAMPLE_11', '1', '2018-09-26', '2018-09-26', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Alfi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (172, '192.168.40.208', 'I-SAMPLE_12', '1', '2018-10-03', '2018-10-03', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Puput', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (173, '192.168.40.207', 'I-SAMPLE_13', '1', '2018-11-19', '2018-11-19', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'cutting', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (174, '192.168.40.209', 'I-SAMPLE_14', '1', '2019-06-10', '2019-06-10', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'satrio', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (175, '192.168.40.178', 'I-SAMPLE_15', '1', '2019-06-10', '2019-06-10', 'PC', 7, 17, 'Windows 7', NULL, NULL, NULL, 'Elis', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (176, '192.168.40.', 'I-SAMPLE_16', '1', '2019-08-21', '2019-08-21', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'Alfa', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (177, '192.168.40.235', 'I-SAMPLE_17', '1', '2019-10-01', '2019-10-01', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'tessa', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (178, '192.168.40.223', 'I-SAMPLE_18', '1', '2020-01-31', '2020-01-31', 'AIO', 1, 17, 'Windows 10', NULL, NULL, NULL, 'P putut', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (179, '192.168.40.', 'I-SAMPLE_19', '1', '2020-01-31', '2020-01-31', 'PC', 7, 17, 'Windows 10', NULL, NULL, NULL, 'Retno', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (180, '192.168.40.231', 'I-SAMPLE_20', '1', '2021-06-30', '2021-06-30', 'PC', 2, 17, 'Windows 10', NULL, NULL, NULL, 'Nur', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (181, '192.168.40.191', 'S-PPIC_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Yani', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (182, '192.168.40.192', 'S-PPIC_2/', '1', '0000-00-00', '0000-00-00', NULL, 1, 6, NULL, NULL, NULL, NULL, 'Ratna ', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (183, '192.168.40.225', 'S-PPIC_3/K-QC_10', '1', '2018-03-11', '2018-03-11', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'siwi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (184, '192.168.40.194', 'S-PPIC_4', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Nurul', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (185, '192.168.40.195', 'S-PPIC_5/QC', '1', '0000-00-00', '0000-00-00', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Isti', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (186, '192.168.40.161', 'W-PACKING_1', '1', '0000-00-00', '0000-00-00', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Ardi ', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (187, '192.168.40.196', 'S-PPIC_6', '1', '2018-06-05', '2018-06-05', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Rina', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (188, '192.168.40.197 ', 'S-PPIC_7', '1', '2018-10-22', '2018-10-22', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Hepi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (189, '192.168.40.198', 'S-PPIC_8', '1', '2018-10-22', '2018-10-22', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Mifta', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (190, '192.168.40.199', 'S-PPIC_9', '1', '2018-10-22', '2018-10-22', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Arvi', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (191, '192.168.40.66', 'S-PPIC_10', '1', '2018-12-16', '2018-12-16', 'AIO', 1, 6, 'Windows 10', NULL, NULL, NULL, 'Iin', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (192, '192.168.40.192', 'N-PLANNING_1', '1', '2019-10-01', '2019-10-01', 'PC', 7, 6, NULL, NULL, NULL, NULL, 'Ratna ', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (193, '192.168.40.198', 'N-PLANNING_2', '1', '2019-10-01', '2019-10-01', 'PC', 7, 6, 'Windows 10', NULL, NULL, NULL, 'Wury', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (194, '192.168.40.61', 'O-MTN_1', '1', '2016-12-16', '2016-12-16', 'PC', 7, 6, 'Windows 7', NULL, NULL, NULL, 'Adm. Mtn', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (195, '192.168.40.251', 'BEA_CUKAI', '1', '2017-06-04', '2017-06-04', 'PC', 7, 23, 'Windows 7', NULL, NULL, NULL, 'Bea Cukai', 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (196, '192.168.40.252', 'BEACUKAI_1', '1', '2017-11-23', '2017-11-23', 'PC', 7, 23, 'Windows 7', NULL, NULL, NULL, NULL, 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');
INSERT INTO `komputers_copy1` VALUES (197, '192.168.40.00', 'SGTC', '1', '2016-12-16', '2016-12-16', 'PC', 7, 2, 'Windows 7', NULL, NULL, NULL, NULL, 1, 'akg', '2021-09-08 12:34:14', '2021-09-08 12:34:14');

-- ----------------------------
-- Table structure for mereks
-- ----------------------------
DROP TABLE IF EXISTS `mereks`;
CREATE TABLE `mereks`  (
  `merek_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merek_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `merek_simbol` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `distributor` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`merek_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mereks
-- ----------------------------
INSERT INTO `mereks` VALUES (1, 'LENOVO', 'LN', 'Bhineka', '2021-09-08 11:14:00', '2021-09-08 11:14:00');
INSERT INTO `mereks` VALUES (2, 'DELL', 'DL', 'Bhineka', '2021-09-08 11:14:00', '2021-09-08 11:14:00');
INSERT INTO `mereks` VALUES (3, 'HP', 'HP', 'Bhineka', '2021-09-08 11:14:00', '2021-09-08 11:14:00');
INSERT INTO `mereks` VALUES (4, 'HPE', 'HPE', 'ACS', '2021-09-08 11:14:00', '2021-09-08 11:14:00');
INSERT INTO `mereks` VALUES (5, 'ZEBRA', 'ZE', 'ACS', '2021-09-08 11:14:00', '2021-09-08 11:14:00');
INSERT INTO `mereks` VALUES (6, 'SOLUTION', 'SL', 'MII', '2021-09-08 11:14:00', '2021-09-08 11:14:00');
INSERT INTO `mereks` VALUES (7, 'Rakitan', 'PCR', 'NUI', NULL, NULL);
INSERT INTO `mereks` VALUES (8, 'Supermicro', 'SM', 'NUI', NULL, NULL);
INSERT INTO `mereks` VALUES (9, 'Intel', 'NUC', 'NUC', NULL, NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (5, '2018_11_30_041232_create_gudang', 2);
INSERT INTO `migrations` VALUES (6, '2018_11_30_041343_create_barang', 2);
INSERT INTO `migrations` VALUES (9, '2021_09_08_033403_create_mereks_table', 3);
INSERT INTO `migrations` VALUES (10, '2021_09_08_033525_create_depts_table', 3);
INSERT INTO `migrations` VALUES (11, '2021_09_08_034031_create_company_table', 3);
INSERT INTO `migrations` VALUES (12, '2021_09_08_082028_create_karyawan_table', 4);
INSERT INTO `migrations` VALUES (14, '2021_09_08_032733_create_komputers_table', 5);

-- ----------------------------
-- Table structure for satuans
-- ----------------------------
DROP TABLE IF EXISTS `satuans`;
CREATE TABLE `satuans`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `satuan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of satuans
-- ----------------------------
INSERT INTO `satuans` VALUES (1, 'LS', 'Lusin', '2021-09-07 21:35:18', '2021-09-07 21:35:18');
INSERT INTO `satuans` VALUES (2, 'GR', 'Gram', '2021-09-07 21:35:18', '2021-09-07 21:35:18');
INSERT INTO `satuans` VALUES (3, 'PCS', 'Piecis', '2021-09-07 21:35:18', '2021-09-07 21:35:18');
INSERT INTO `satuans` VALUES (4, 'PK', 'Pack', '2021-09-07 21:35:18', '2021-09-07 21:35:18');
INSERT INTO `satuans` VALUES (5, 'BH', 'Buah', '2021-09-07 21:35:18', '2021-09-07 21:35:18');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Angun Helpdesk', 'it@anggunkreasi.com', NULL, '$2y$10$tiJqCZxVxy9BnKIuUCQGKOrQRMDdxTTPdjt5BCCiAxqZrdjrZrcn.', 'xz1dSmFPWkO66uxmETxpC2H8lIAFneKG5kRG2RmnbdFqmfsC3o2q5BIsTL3y', '2021-09-07 10:16:14', '2021-09-07 10:16:14');

SET FOREIGN_KEY_CHECKS = 1;
