@extends('layouts.app')

@section('content')

<div class="content">
	<div class="content-header">
		<div class="page-title">
			 
		</div>
		<div class="page-breadcumb">
			
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
			    <li class="breadcrumb-item"><a href="#">Home</a></li>
			    <li class="breadcrumb-item active" aria-current="page">Komputer Asset</li>
			  </ol>
			</nav>
		</div>
	</div>			
	<div class="content-body">
		
		<section  class="chart">
			<div class="panel">
				<div class="panel-header d-flex align-items-center justify-content-between">
						<div class="panel-title">
							<i class="fa fa-book"></i> 	   Daftar Komputer Asset
						</div>
						<div>
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="fa fa-plus"></i></button>
						</div>
				</div>
				<div class="panel-body">
					<div class="row">
						@if(Session::has('success_add'))
						<div class="col-12">
							<div class="alert alert-success" role="alert">
								{{ Session::get('success_add') }}
							</div>
						</div>
						@endif
					</div>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>IP</th>
									<th>Name</th>
									<th>User</th>
									<th>Pembelian</th>
									<th>Setup</th>
									<th>Model</th>
									<th>Merek</th>
									<th>Dept</th>
									<th>OS</th>
									<th>Remote ID</th>
									<th>Mac </th>
									<th>Email</th>
									<th>Note</th>
									<th>Company</th>						 
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($komputers as $komputer)
								<tr>
									<td>{{$loop->iteration}}</td> 
									<td>{{$komputer->ip_pc}}</td> 
									<td>{{$komputer->comp}}</td> 
									<td>{{$komputer->user_id}}</td>  
									<td>{{$komputer->date_buy}}</td>  
									<td>{{$komputer->date_setup}}</td>  
									<td>{{$komputer->model}}</td> 
									<td>{{$komputer->merek_id}}</td>  
									<td>{{$komputer->dept_id}}</td>  
									<td>{{$komputer->os_build}}</td> 
									<td>{{$komputer->remote}}</td> 
									<td>{{$komputer->macaddress}}</td> 
									<td>{{$komputer->email}}</td> 
									<td>{{$komputer->note}}</td>  
									<td>{{$komputer->company_id}}</td>
									<td>
										<a href="barang/edit/{{$komputer->id}}"><i class="fa fa-edit"></i> </a>
										<a href="barang/delete/{{$komputer->id}}"><i class="fa fa-trash"></i> </a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>			
</div>
<div class="modal fade tambah-exam" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Komputer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form method="POST" action="{{url('komputer/')}}">
        	@csrf()
	      <div class="modal-body">
		  
		  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">IP:</label>
	            <input class="form-control" Placeholder="192.168.10.1" name="ip_pc"></input>
	          </div>


		 	 <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Name:</label>
	            <input class="form-control" name="comp"></input>
	          </div>

			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">User:</label>
	            <select class="form-control" name="user_id">
	            	<option>Pilih User..</option>
	            	@foreach($karyawans as $karyawan)
	            	<option value="{{$karyawan->id}}">{{$karyawan->kar_name}}</option>
	            	@endforeach
	            </select>
	          </div>

			  
			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Tgl Pembelian:</label>
	            <input class="date form-control" type="date" name="date_buy"></input>
	          </div>

			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Tgl Setup:</label>
	            <input class="date form-control" type="date" name="date_setup"></input>
	          </div>
 
			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Model:</label>
	            <select class="form-control" name="model">
	            	<option>Pilih  Model..</option>	            	 
	            	<option value="AIO"> AIO</option>
					<option value="PC Builup"> PC Builup</option>
					<option value="PC RAKITAN"> PC RAKITAN</option>
					<option value="Laptop"> Laptop</option>
					<option value="Mini PC"> Mini PC</option>
					<option value="Other"> Other</option>
	            </select>
	          </div>

			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Brand:</label>
	            <select class="form-control" name="satuan">
	            	<option>Pilih salah Brand..</option>
	            	@foreach($mereks as $merek)
	            	<option value="{{$merek->merek_id}}">{{$merek->merek_name}}</option>
	            	@endforeach
	            </select>
	          </div>


	          <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Departement:</label>
	            <select class="form-control" name="merek_simbol">
	            	<option>Pilih Departemen..</option>
	            	@foreach($depts as $dept)
	            	<option value="{{$dept->id}}">{{$dept->dept_name}}</option>
	            	@endforeach
	            </select>
	          </div>
			
			  
	          <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Os version:</label>
	            <select class="form-control" name=">os_build">
	            	<option>Pilih OS Version..</option>
	            	<option value="win7">Windows 7</option>
					<option value="win8">Windows 8</option>
					<option value="win10">Windows 10</option>
					<option value="win11">Windows 11</option>
					<option value="linux">Linux</option>
	            	 
	            </select>
	          </div>

			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">Remote:</label>
	            <input class="form-control" name="remote"></input>
	          </div>

			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">macaddress:</label>
	            <input class="form-control" name="macaddress"></input>
	          </div>

			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">email:</label>
	            <input class="form-control" name="email"></input>
	          </div>

			  <div class="form-group">
	            <label for="recipient-name" class="col-form-label">note:</label>
	            <input class="form-control" name="note"></input>
	          </div>


	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Tambah</button>
	      </div>
       </form>
    </div>
  </div>
</div>
@endsection