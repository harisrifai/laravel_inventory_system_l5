<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Information Management System (Anggun)</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="https://fonts.gstatic.com"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css"> -->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/waves.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('fonts/fa/css/font-awesome.min.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>



</head>
<body>
    <div id="app">  
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="logo d-flex justify-content-center">
                <a class="navbar-brand" href="{{ url('/') }}">
                     
                </a>
            </div>
                

                      

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <i class="fa fa-user"></i>  {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                
                                <a class="dropdown-item" href="<?php echo url('/') ?>/komputer" class="side-link waves-effect">
                                <i class="fa fa-users"></i><span> Komputer</span> </a>
                                
                                <a class="dropdown-item" href="<?php echo url('/') ?>/gudang" class="side-link waves-effect">
                                <i class="fa fa-book"></i><span> Gudang</span> </a>

                                <a class="dropdown-item" href="<?php echo url('/') ?>/barang" class="side-link waves-effect">
                                <i class="fa fa-user"></i> <span> Barang</span> </a>
                                
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="fa fa-users"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                
        </nav>

        

        <div id="content" class="col-md-15">
                @yield('content')
            </div>
         
    </div>
    <script type="text/javascript" src="{{  asset('js/jquery.min.js') }}"></script>
     <script src="{{ asset('js/app.js') }}" defer></script>
    <script type="text/javascript" src="{{  asset('js/waves.min.js') }}"></script>
    <script type="text/javascript" src="{{  asset('js/raphael.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('lib/datatables.min.js') }}" defer></script>
    <script type="text/javascript" src="{{  asset('js/custom.js') }}"></script>
    
     

    <footer>
         Simple Use &hearts;  
    </footer>
</body>
</html>
        